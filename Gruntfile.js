module.exports = function(grunt) {

  grunt.initConfig({

    requirejs: {
      compile: {
        options: {
          mainConfigFile: 'build.js'
        }
      }
    },

    // minify the optimized library file
    uglify: {
      dist: {
        files: {
          'dist/btsdk.min.js': ['dist/btsdk.js']
        }
      }
    },

    // kick off jasmine, showing results at the cli
    jasmine: {
      taskName: {
        options: {
          outfile: 'test/runner.html',
          keepRunner: true,
          specs: 'test/spec/*.js',
          template: require('grunt-template-jasmine-requirejs'),
          templateOptions: {
            requireConfig: {
              baseUrl: '../lib', // relative to runner.html location
              paths: {
                'bean':            '../vendor/bean',
                'dragdealer':      '../vendor/dragdealer',
                'hark':            '../vendor/hark',
                'jquery':          '../vendor/jquery-2.1.3.min',
                'jquery-slideout': '../vendor/jquery.tabslideout.1.3',
                'jquery-ui':       '../vendor/jquery-ui.min',
                'jssip':           '../vendor/jssip',
                'moment':          '../vendor/moment',
                'reconnecting-ws': '../vendor/reconnecting-websocket',
                'reqwest':         '../vendor/reqwest',
                'sipjs':           '../vendor/sipjs',
                'sipml5':          '../vendor/sipml5',
                'tock':            '../vendor/tock',
                'underscore':      '../vendor/underscore'
              }
            }
          }
        }
      }
    },
    exec: {
      zip: {
        stdout: true,
        stderr: true,
        cmd: function (version) {
          var dir = 'btsdk-' + version;

          return [
            'mkdir ' + dir,
            'mkdir ' + dir + '/example',
            'cp dist/btsdk.js ' + dir + '/btsdk-' + version + '.js',
            'cp dist/btsdk.min.js ' + dir + '/btsdk-' + version + '.min.js',
            'cp example/sip.html ' + dir + '/example/sip.html',
            'cp example/sos.php ' + dir + '/example/sos.php',
            'cp example/user.html ' + dir + '/example/user.html',
            'zip -r btsdk-' + version + '.zip ' + dir,
          ].join('&&');
        }
      }
    },

  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('test', 'jasmine');
  grunt.registerTask('build', ['requirejs', 'uglify']);
  grunt.registerTask('default', ['jasmine', 'requirejs', 'uglify']);
};
