define([
  'btsdk/core',
  'btsdk/config',
  'btsdk/sip',
  'btsdk/sos/skin/default',
  'btsdk/sos/skin/video',
  'bean'
], function (core, config, sip, skin, videoSkin, bean) {
  'use strict';

  return {

    initialized: false,

    /**
     * Returns whether the browser is WebRTC capable.
     *
     * @return {bool}
     */
    acceptableBrowser: function () {
      return !!window.RTCPeerConnection;
    },

    /**
     * Initializes the skin and any listeners.
     *
     * @return {none}
     */
    init: function (opts) {
      console.log('[sos] - init');

      core.loadConfig(opts);

      if (config.video) {
        this.skin = videoSkin;
      }
      else {
        this.skin = skin;
      }

      this.skin.init();
      this.initialized = true;
      bean.fire(core, 'btsdk.sos.initialized');
    },

    /**
     * Destroys the SIP module, the skin, and removes event listeners.
     *
     * @return {none}
     */
    destroy: function () {
      if (!this.initialized) return;

      sip.destroy();
      this.skin.destroy();

      bean.fire(core, 'btsdk.sos.destroyed');
      this.initialized = false;
    },

    /**
     * Updates the single-use SOS token.
     *
     * @param  {string} token
     * @return {none}
     */
    updateToken: function (token) {
      console.log('[sos] - updating sos token to ', token);

      // update sos config & sip engine with new token
      config.sos.token = token;
      sip.updateConfig('password', token);

      // re-register once token set
      // sip.register(); // do NOT register SOS EPAs
    },

    error: function (msg) {
      bean.fire(core, 'btsdk.sos.error', {
        msg: msg
      });

      this.skin.error(msg);
    },

    /**
     * Returns the value of the `Call-ID` header from the SIP module
     * if a call is currently up.
     *
     * @return {string|null}
     */
    getCallID: function () {
      return sip.getCallID();
    },

    /**
     * Returns all call-attached-data.
     *
     * @return {jqXHR}
     */
    getCallAttachedData: function () {
      var cid   = this.getCallID(),
          epa   = config.address + '@edge.' + config.env,
          token = config.sos.token;

      return core.api.edge.fetchAllCallAttachedData(cid, epa, token);
    },

    /**
     * Sets a new key-value-pair for call-attached-data.
     *
     * @return {jqXHR}
     */
    setCallAttachedData: function (key, value) {
      var cid   = this.getCallID(),
          epa   = config.address + '@edge.' + config.env,
          token = config.sos.token;

      return core.api.edge.setCallAttachedData(cid, epa, token, key, value);
    },

  };
});