define([
  'btsdk/core',
  'btsdk/api',
  'bean',
  'moment',
  'underscore'
], function (core, api, bean, moment, _) {
  'use strict';

  return {

    tokenCheckMin: 5,

    tokenRefreshMin: 15,

    authUser: null,

    token: null,

    endpoints: null,

    updateIntervalID: null,

    _loadToken: function(token, triggerAuthenticate) {
      if (!token.proxyToken || !token.renewTS || !token.expireTS || !this._validToken(token)) {
        setTimeout((function () {
          // ensure timeout fires after client has chance to attach listeners
          bean.fire(core, 'btsdk.user.token_invalid');
        }).bind(this), 0);
        return;
      }

      this.token    = _.pick(token, 'proxyToken', 'renewTS', 'expireTS');
      this.authUser = _.pick(token, 'user').user;


      if (triggerAuthenticate) {
        setTimeout((function () {
          // ensure timeout fires after client has chance to attach listeners
          // when loading a token on btsdk.init()
          bean.fire(core, 'btsdk.user.authenticate', this.authUser);
        }).bind(this), 0);
      }

      if (this.updateIntervalID) {
        clearInterval(this.updateIntervalID);
        this.updateIntervalID = null;
      }

      this.tokenUpdateCheck();
      this.updateIntervalID = setInterval(this.tokenUpdateCheck.bind(this), (this.tokenCheckMin * 60 * 1000));
    },

    _validToken: function (token) {
      var expires = new Date(token.expireTS).getTime();
      var renews  = new Date(token.renewTS).getTime();

      if (expires - (new Date().getTime()) < 0) return false;
      if (renews - (new Date().getTime()) < 0) return false;

      return true;
    },

    destroy: function () {
      this.authUser  = null;
      this.token     = null;
      this.endpoints = null;

      if(this.updateIntervalID) {
        clearInterval(this.updateIntervalID);
        this.updateIntervalID = null;
      }
    },

    /**
     * Authenticates a user by attempting to create a proxy token.
     *
     * @param  {string} username
     * @param  {string} password
     * @return {promise}
     */
    authenticate: function (username, password) {
      var self = this;

      return api.cfg.createProxyToken(username, password)
        .then(function (resp) {
          console.log('[user] - login success',resp.resultResponses);

          self._loadToken(resp.resultResponses, true);
          bean.fire(core, 'btsdk.user.token_create', resp.resultResponses);
        })
        .fail(function (xhr) {
          bean.fire(core, 'btsdk.user.error', {
            error: 'Failed to log in',
            eventTS: new Date().toISOString()
          });
        });
    },

    /**
     * Checks the user's proxy token.
     * If it is within `tokenRefreshMin` minutes of expiring, it will renew.
     *
     * @return {void}
     */
    tokenUpdateCheck: function() {
      var now   = moment.utc(),
          renew = moment(this.token.renewTS).utc();

      if(renew.diff(now, 'minutes') < this.tokenRefreshMin) {
        console.log('[user] - proxyToken: renewing token');

        this.updateProxyToken();
      } else {
        console.log('[user] - proxyToken: token expires in ' + renew.diff(now, 'minutes') + ' minutes');
      }
    },

    /**
     * Renews the user's proxy token.
     *
     * @return {void}
     */
    updateProxyToken: function () {
      console.log('[user] - updateProxyToken');

      var self = this,
          xhr  = api.cfg.renewProxyToken();

      xhr.then(function (resp) {
        self._loadToken(resp.resultResponses);

        bean.fire(core, 'btsdk.user.token_renew', {
          eventTS: new Date().toISOString(),
          token: self.token
        });
      })
      .fail(function (xhr) {
        if(self.updateIntervalID) {
          clearInterval(self.updateIntervalID);
          self.updateIntervalID = null;
        }

        bean.fire(core, 'btsdk.user.token_renew_error', {
          error: 'Failed to renew proxy token',
          eventTS: new Date().toISOString()
        });
      });
    },

    /**
     * Loads the user's endpoints.
     *
     * @return {promise}
     */
    loadEndpoints: function () {
      var self = this;

      return api.cfg.endpointList(self.authUser.userID)
        .then(function (resp) {
          self.endpoints = resp.endpointList.endpoint;
          bean.fire(core, 'btsdk.user.endpoints_loaded', { endpoints: self.endpoints });
        })
        .fail(function (xhr) {
          bean.fire(core, 'btsdk.user.error', {
            error: 'Failed to fetch user endpoints',
            eventTS: new Date().toISOString()
          });
        });
    },

    /**
     * Loads the user's speed dial list.
     *
     * @return {promise}
     */
    loadSpeedDial: function () {
      var self = this;

      return api.cfg.speedDialList()
        .then(function (resp) {
          self.speedDial = resp.speedDialList.speedDial.length > 0 ? resp.speedDialList.speedDial[0] : null;
          bean.fire(core, 'btsdk.user.speed_dial_loaded', { speedDial: self.speedDial });
        })
        .fail(function (xhr) {
          bean.fire(core, 'btsdk.user.error', {
            error: 'Failed to fetch speed dial',
            eventTS: new Date().toISOString()
          });
        });
    },

    /**
     * Loads the user's interface profile.
     *
     * @return {promise}
     */
    loadInterfaceProfile: function () {
      var self = this;

      return api.cfg.interfaceProfileList(self.authUser.userID)
        .then(function (resp) {
          self.interfaceProfile = resp.interfaceProfileList.interfaceProfile.length > 0 ? resp.interfaceProfileList.interfaceProfile[0] : null;
          bean.fire(core, 'btsdk.user.interface_profile_loaded', { interfaceProfile: self.interfaceProfile });
        })
        .fail(function (xhr) {
          bean.fire(core, 'btsdk.user.error', {
            error: 'Failed to fetch interface profile',
            eventTS: new Date().toISOString()
          });
        });
    },

    /**
     * Loads the relevant queues for a user's skills.
     *
     * @return {promise}
     */
    loadQueueList: function () {
      var self = this;

      if (this.authUser.skillList.skill.length === 0) {
        // user has no skills, therefore no associated queues
        bean.fire(core, 'btsdk.user.queues_loaded', { queues: [] });
        return;
      }

      var queueIDs = _.reduce(this.authUser.skillList.skill, function (acc, skill) {
        return acc.concat(skill.queueList.queueID);
      }, []);

      queueIDs = _.uniq(queueIDs);

      var reqs = _.map(queueIDs, function (queueID) {
        return api.cfg.queueList({queueID: queueID});
      });

      var all = Promise.all(reqs);

      all.then(function () {
        var args = Array.prototype.slice.call(arguments);
        var queues = _.map(args[0], function (resp) {
          return resp.queueList.queue[0];
        });

        bean.fire(core, 'btsdk.user.queues_loaded', { queues: queues });
      }, function () {
        bean.fire(core, 'btsdk.user.error', {
          error: 'Failed to fetch queue list',
          eventTS: new Date().toISOString()
        });
      });

      return all;
    },

    /**
     * Subscribes to the user's API-INT events.
     *
     * @return {void}
     */
    subscribe: function () {
      api.int.subscribe({ userID: this.authUser.userID });
    },

    /**
     * Un-subscribes to the user's API-INT events.
     *
     * @return {void}
     */
    unsubscribe: function () {
      api.int.unsubscribe({ userID: this.authUser.userID });
    },

    /**
     * Requests the user's status.
     *
     * @return {void}
     */
    status: function () {
      api.int.userStatus(this.authUser.userID);
    },

    /**
     * Logs the user in for state management.
     *
     * @param  {string} endpointAddressID
     * @return {promise}
     */
    stateLogin: function (endpointAddressID) {
      api.int.stateLogin({
        userID: this.authUser.userID,
        endpointAddressID: endpointAddressID
      });
    },

    /**
     * Logs the user out from state management.
     *
     * @return {promise}
     */
    stateLogout: function () {
      api.int.stateLogout(this.authUser.userID);
    },

    /**
     * Logs the user in for MCB.
     *
     * @param  {string} endpointAddressID
     * @return {void}
     */
    mcbLogin: function (endpointAddressID) {
      api.int.mcbLogin({
        userID: this.authUser.userID,
        endpointAddressID: endpointAddressID
      });
    },

    /**
     * Logs the user out from MCB.
     *
     * @return {void}
     */
    mcbLogout: function () {
      api.int.mcbLogout(this.authUser.userID);
    },

    /**
     * Requests the available user-states.
     *
     * @return {promise}
     */
    availableStates: function () {
      api.int.availableStates(this.authUser.userID);
    },

    /**
     * Requests a state change for the user.
     *
     * @param  {string} stateID
     * @param  {string} subStateID
     * @return {void}
     */
    stateChange: function (stateID, subStateID) {
      api.int.stateChange({
        userID: this.authUser.userID,
        stateID: stateID,
        subStateID: subStateID
      });
    }

  };
});