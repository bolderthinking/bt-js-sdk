define(function (require) {
  'use strict';

  var _      = require('underscore'),
      bean   = require('bean'),
      config = require('btsdk/config');

  var btsdk = (function () {
    function btsdk() {
      var self = this;

      this.VERSION = '2.18.0';

      // non-amd js requires init function
      if (typeof window.btAsyncInit === 'function') {
        // only run once lib is init'd and in global scope
        setTimeout(function () {
          console.log('[core] - btAsyncInit');
          window.btAsyncInit();
        },0);
      }

      this.init = function (opts) {
        console.log('[core] - init');
        this.loadConfig(opts);
      };

      this.destroy = function () {
        if (self.sip.initialized) {
          self.sip.destroy();
        }

        if (self.sos.initialized) {
          self.sos.destroy()
        }

        if (this.api.int.ws) {
          this.api.int.wsClose();
        }

        self.user.destroy();
        bean.off(this);
      };

      this.loadConfig = function (opts) {
        console.log('[core] - loadConfig');

        opts = opts || {};

        if (opts.address && opts.address.indexOf('@') !== -1) {
          if (opts.env) {
            if (opts.env != opts.address.split('@edge.')[1]) {
              throw '[core] - incompatible endpoint address domain and `env` init opt';
            }
          }
          else {
            opts.env = opts.address.split('@edge.')[1];
          }

          opts.address = opts.address.split('@')[0];
        }

        config = _.extend(config, opts);

        this.addListeners(opts);

        if (opts.user) {
          console.log('[core] - loading init proxy token');
          self.user._loadToken(opts.user, true);
        }
      };

      this.addListeners = function (opts) {
        if (!opts.listeners) return;

        for (var eventName in opts.listeners) {
          bean.on(this, eventName, opts.listeners[eventName]);
        }
      };

      this.on = function (eventName, cb) {
        bean.on(this, eventName, cb);
      };

      this.off = function (eventName) {
        bean.off(this, eventName);
      };
    }

    return btsdk;
  })();

  return new btsdk();
});