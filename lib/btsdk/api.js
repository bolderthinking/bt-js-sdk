define([
  'btsdk/api/cfg',
  'btsdk/api/edge',
  'btsdk/api/int',
  'btsdk/api/rpt',
], function (cfg, edge, int, rpt) {
  'use strict';

  return {
    cfg:  cfg,
    edge: edge,
    int:  int,
    rpt:  rpt
  };
});