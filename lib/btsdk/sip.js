define([
  'btsdk/core',
  'btsdk/config',
  'btsdk/sip/jssip',
  // 'btsdk/sip/sipml5',
  // 'btsdk/sip/sipjs',
  'bean'
], function(core, config, jssip, /* sipml, sipjs, */ bean) {
  'use strict';

  return {
    initialized: false,

    engine: null,

    /**
     * Initializes the SIP engine.
     *
     * @return {none}
     */
    init: function(opts) {
      console.log('[sip] - init');

      core.loadConfig(opts);

      switch(config.engine) {
        case 'sipml5':
          // this.engine = sipml;
          throw "Unsupported engine `sipml5`";
          break;
        case 'sipjs':
          // this.engine = sipjs;
          throw "Unsupported engine `sipjs`";
          break;
        case 'jssip':
        default:
          this.engine = jssip;
      }

      var realm = 'edge.' + config.env,
          wss   = 'wss://api-edge.' + config.env + '/edge/ws',
          impi  = config.address,
          impu  = 'sip:' + config.address + '@' + realm,
          pass  = config.sos
            ? config.sos.token
            : config.password;

      this.engine.init({
        websocket: wss,
        realm: realm,
        impi: impi,
        impu: impu,
        pass: pass
      });

      this.initialized = true;
    },

    /**
     * Updates an engine config property.
     *
     * @param  {string} key
     * @param  {mixed} value
     * @return {none}
     */
    updateConfig: function(key, value) {
      console.log('[sip] - updating config', key, value);

      this.engine.updateConfig(key, value);
    },

    /**
     * Sends a SIP Register.
     *
     * @return {none}
     */
    register: function() {
      console.log('[sip] - register');

      this.engine.register();
    },

    /**
     * Sends a SIP Invite.
     *
     * @return {none}
     */
    call: function(number) {
      console.log('[sip] - call');

      this.engine.call(number);
    },

    answer: function () {
      console.log('[sip] - answer');

      this.engine.answer();
    },

    /**
     * Sends a SIP Hangup.
     *
     * @return {none}
     */
    hangup: function() {
      console.log('[sip] - hangup');

      this.engine.hangup();
    },

    /**
     * Destroys the SIP engine and removes all event listeners.
     *
     * @return {none}
     */
    destroy: function() {
      console.log('[sip] - destroy')
      if (!this.initialized) return;

      this.engine.destroy();

      bean.fire(core, 'btsdk.sip.destroyed');
      this.initialized = false;
    },

    /**
     * Mutes the outbound microphone.
     *
     * @return {none}
     */
    muteMic: function() {
      console.log('[sip] - mute mic')

      this.engine.muteMic();
    },

    /**
     * Un-mutes the outbound microphone.
     *
     * @return {none}
     */
    unmuteMic: function () {
      console.log('[sip] - unmute mic')

      this.engine.unmuteMic();
    },

    micStats: function () {
      return this.engine.micStats();
    },

    /**
     * Returns the current SIP call ID.
     *
     * @return {string}
     */
    getCallID: function() {
      return this.engine.currentCallID;
    },

    /**
     * Returns all call-attached-data.
     *
     * @return {promise}
     */
    getCallAttachedData: function () {
      var cid   = this.getCallID(),
          epa   = config.address + '@edge.' + config.env,
          pass  = config.password;

      return core.api.edge.fetchAllCallAttachedData(cid, epa, pass);
    },

    /**
     * Sets a new key-value-pair for call-attached-data.
     *
     * @return {promise}
     */
    setCallAttachedData: function (key, value) {
      var cid   = this.getCallID(),
          epa   = config.address + '@edge.' + config.env,
          pass  = config.password;

      return core.api.edge.setCallAttachedData(cid, epa, pass, key, value);
    },

    /**
     * Returns all system-public-data.
     *
     * @return {promise}
     */
    getSystemPublicData: function () {
      var cid   = this.getCallID(),
          epa   = config.address + '@edge.' + config.env,
          pass  = config.password;

      return core.api.edge.fetchAllSystemPublicData(cid, epa, pass);
    },

  };

});