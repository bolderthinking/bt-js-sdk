define([
  'btsdk/core',
  'btsdk/config',
  'btsdk/sip',
  'jquery',
  'bean',
  'dragdealer',
  'tock',
  'jquery-ui',
  'jquery-slideout'
], function(core, config, sip, $, bean, dragdealer, tock) {
  'use strict';

  return {
    _tock: null,

    msg_unsupportedBrowser: 'Your browser is not supported.<br>Please use latest versions of Chrome or Firefox.',

    msg_micPermissionRefused: 'You must grant use of your microphone to continue.',

    muted : false,

    errorHtml: function(msg) {
      return '\
        <div class="alert alert-danger alert-dismissible" role="alert">\
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span aria-hidden="true">&times;</span>\
          </button>'
          + msg +
        '</div>';
    },

    widget: function() {
      return '\
    <div id="webRTCwidget" class="slide-out-div">\
      <a class="handle" href="http://link-for-non-js-users.html">\
        <span><img id="headset-img" src="//' + config.cdn + '/assets/img/headset-icon.png"></span>\
        <span id="tab-text">Talk</span>\
      </a>\
      <div class="top-box">\
        <button id="tlk-now-btn">Talk Now</button>\
        <img class="status-gif" src="//' + config.cdn + '/assets/img/black-wifi-icon-hi.gif" style="height: 69px; padding-top: 20px; padding-left: 104px;">\
        <p style="margin:0;top:65px;left:20px;position:absolute;font-weight:bold;">(Your browser will ask you to confirm use of your microphone)</p>\
      </div>\
      <div class="toolbar">\
        <div class="tlbr-block-time">\
          <div id="bt-sos-timer" class="time">00:00</div>\
        </div>\
        <div class="tlbr-block mic"></div>\
        <div class="tlbr-block spkr"></div>\
        <div class="ctrl-box ctrl-spkr">\
          <div class="slider-box">\
            <div id="slider-spkr" style="height: 75px;left: 32px;top: 12px;"></div>\
          </div>\
        </div>\
        <div class="tlbr-block end"></div>\
        <div class="ctrl-box ctrl-end" style="z-index:999;">\
          <button id="end-call-btn">End Call</button>\
        </div>\
      </div>\
      <div style="width: 100%; margin: 10px 0 10px 0;">\
        <video id="btsdk-stream-remote" autoplay _hidden=true poster="//placehold.it/147x110.png/000/fff&text=Remote" style="display: inline-block; height: 110px; width: 147px;"></video>\
        <video id="btsdk-stream-local" autoplay _hidden=true poster="//placehold.it/147x110.png/000/fff&text=Local" style="display: inline-block; height: 110px; width: 147px;"></video>\
      </div>\
      <audio id="ringbacktone" loop="" src="//' + config.cdn + '/assets/ring.wav"></audio>\
    </div>';
    },

    css: [
      'default.css'
    ],

    init: function() {
      this.loadStyles();
      this.cssReady(this.insertWidget.bind(this));
    },

    el: function() {
      return document.getElementById(config.sos.selectorId);
    },

    loadStyles: function() {
      for(var i in this.css) {
        var link = document.createElement('link');

        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = '//' + config.cdn + '/assets/css/skins/' + this.css[i];

        var entry = document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(link, entry);
      }
    },

    cssReady: function(callback) {
      var testElem = document.createElement('span');
      testElem.id = 'bt-sos-skin-css-ready';
      testElem.style.color = '#fff';

      var entry = document.getElementsByTagName('script')[0];
      entry.parentNode.insertBefore(testElem, entry);

      (function poll() {
        var node = document.getElementById('bt-sos-skin-css-ready');
        var value;

        if(window.getComputedStyle) {
          value = document.defaultView
                          .getComputedStyle(testElem, null)
                          .getPropertyValue('color');
        } else if(node.currentStyle) {
          value = node.currentStyle.color;
        }

        if(value && value === 'rgb(186, 218, 85)' || value.toLowerCase() === '#bada55') {
          callback();
        } else {
          setTimeout(poll, 100);
        }
      })();
    },

    cssModifications: function() {
      // the hostname of these imgs needs to be dynamic
      $('.tlbr-block.mic').css({
        'background-image': 'url(//' + config.cdn + '/assets/img/glyphicons_300_microphone.png)',
        'background-repeat': 'no-repeat',
        'background-position': 'center'
      });
      $('.tlbr-block.spkr').css({
        'background-image': 'url(//' + config.cdn + '/assets/img/glyphicons_184_volume_up.png)',
        'background-repeat': 'no-repeat',
        'background-position': 'center'
      });
      $('.tlbr-block.end').css({
        'background-image': 'url(//' + config.cdn + '/assets/img/phone.png)',
        'background-repeat': 'no-repeat',
        'background-position': 'center'
      });
    },

    insertWidget: function() {
      this.el().innerHTML = this.widget();
      this.skinSetup();
    },

    skinSetup: function() {
      var self = this;

      // initialize sip engine
      sip.init();

      // alter css asset urls
      self.cssModifications();

      document.getElementById('ringbacktone').volume = 0.7;

      // widget controls
      $('.slide-out-div').tabSlideOut({});
      $('.slide-out-div').show();
      $('.status-gif').hide();

      // click handlers for the widget itself
      $('a.handle').click(function(){
        $(this).hide();
        $('div.ctrl-mic').hide();
        $('div.ctrl-spkr').hide();
        $('div.ctrl-end').hide();
      });

      $(document).click(function(){
        $('a.handle').show();
      });

      $('#tlk-now-btn').click(function() {
        // var userNum = document.getElementById('opt-number').value;
        var userNum = null;
        var dialNum = (!userNum || userNum == '') ? config.sos.number : userNum;

        sip.call(dialNum);
      });

      $('#end-call-btn').click(function() {
        sip.hangup();
      });

      $('.tlbr-block.mic').click(function () {

        if(self.muted === true) {
          sip.unmuteMic();

          // update mic icon
          $('.tlbr-block.mic').css({
            'background-image': 'url(//' + config.cdn + '/assets/img/glyphicons_300_microphone.png)',
            'background-repeat': 'no-repeat',
            'background-position': 'center'
          });
        } else {
          sip.muteMic();

          // update mic icon
          $('.tlbr-block.mic').css({
            'background-image': 'url(//' + config.cdn + '/assets/img/glyphicons_300_muted_microphone.png)',
            'background-repeat': 'no-repeat',
            'background-position': 'center'
          });
        }

        // update muted status
        self.muted = !self.muted;
      });

      $('.tlbr-block.spkr').click(function(){
        $('.ctrl-spkr').toggle();
        $('div.ctrl-mic').hide();
        $('div.ctrl-end').hide();
      });

      $('.tlbr-block.end').click(function(){
        $('.ctrl-end').toggle();
        $('div.ctrl-spkr').hide();
        $('div.ctrl-mic').hide();
      });

      $("#slider-spkr").slider({
        orientation: "vertical",
        range: "min",
        min: 0,
        max: 100,
        value: 100,
        slide: function(event, ui) {
          var value = $("#slider-spkr").slider("value");
          document.getElementById("btsdk-stream-remote").volume = (value / 100);
        },
        change: function() {
          var value = $("#slider-spkr").slider("value");
          document.getElementById("btsdk-stream-remote").volume = (value / 100);
        }
      });

      var doHangup = function() {
        self.timerStop();

        // ensure ringbacktone isn't playing
        // can happen if call never gets established, but hangup still fires
        try {
          document.getElementById('ringbacktone').pause();
          document.getElementById('ringbacktone').currentTime = 0;
        } catch(e) {}

        $('.status-gif').hide();
        $('.status-gif').attr('src', '//' + config.cdn + '/assets/img/black-wifi-icon-hi.gif');
        $('.ctrl-end').hide();
        $('.top-box p').show();
        $('#tlk-now-btn').show();

        // reset mute bits
        self.muted = false;

        $('.tlbr-block.mic').css({
          'background-image': 'url(//' + config.cdn + '/assets/img/glyphicons_300_microphone.png)',
          'background-repeat': 'no-repeat',
          'background-position': 'center'
        });

        // reset video panels
        if(config.video) {
          document.getElementById('btsdk-stream-remote').src = '';
          document.getElementById('btsdk-stream-local').src = '';
        }
      };

      bean.on(core, 'btsdk.sip.call_connected', function(e) {
        console.log('[sos-video] - call connected');

        $('.status-gif').attr('src', '//' + config.cdn + '/assets/img/phone-headset-icon.png');

        try {
          document.getElementById('ringbacktone').pause();
          document.getElementById('ringbacktone').currentTime = 0;
        } catch(e) {}
      });

      bean.on(core, 'btsdk.sip.hangup', function(e) {
        console.log('[sos-video] - received hangup event');

        doHangup();
      });

      bean.on(core, 'btsdk.sip.mic_permission_accepted', function(e) {
        $('#tlk-now-btn').hide();
        $('div.ctrl-mic').hide();
        $('div.ctrl-spkr').hide();
        $('div.ctrl-end').hide();
        $('.top-box p').hide();
        $('.status-gif').show();

        self.timerStart();

        try {
          document.getElementById('ringbacktone').play();
        } catch(e) {}
      });

      bean.on(core, 'btsdk.sip.mic_permission_refused', function(e) {
        core.sos.error(self.msg_micPermissionRefused);
        doHangup();
      });
    },

    tock: function() {
      var self = this;

      if(self._tock === null) {
        self._tock = new tock({
          interval: 1000,
          callback: function() {
            if(!self._tock.isRunning()) return false;
            var elapsed = self._tock.msToTime(self._tock.lap()).replace(/\.\d+/g, '');
            document.getElementById('bt-sos-timer').innerHTML = elapsed;
          }
        });
      }

      return self._tock;
    },

    destroy: function() {
      this.timerStop();
      this.el().innerHTML = '';
    },

    timerStart: function() {
      this.tock().start();
    },

    timerStop: function() {
      this.tock().stop();
      this.tock().reset();

      setTimeout(function() {
        if(document.getElementById('bt-sos-timer')) {
          document.getElementById('bt-sos-timer').innerHTML = '00:00';
        }
      },0);
    },

    error: function(msg) {
      if(!config.sos.errSelectorId) return;
      $('#'+config.sos.errSelectorId).append(this.errorHtml(msg));
    },

  };
});