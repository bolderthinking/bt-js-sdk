define([
  'btsdk/core',
  'btsdk/config',
  'sipml5',
  'bean'
], function (core, config, sipml, bean) {
  'use strict';

  return {
    name: 'sipml5',

    sipStack: null,

    registerSession: null,

    callSession: null,

    msgSession: null,

    sosKeepaliveID: null,

    sosKeepaliveInterval: 60000, // 60 seconds

    currentState: null,

    currentCallID: null,

    stackEventListener: function (e) {
      var self = this;
      // console.log('[sipml] - stack event',e);

      switch(e.type) {
        case 'starting':
          break;
        case 'started':
          // self.startSosKeepalive();

          bean.fire(core, 'btsdk.sip.initialized');
          break;
        case 'failed_to_start':
        case 'failed_to_stop':
          console.error('[sipml] - failed ',e.type,e);
          bean.fire(core, 'btsdk.sip.error', e);
          // INTENTIONAL omitted `break`
        case 'stopping':
        case 'stopped':
          self.callSession     = null;
          self.registerSession = null;
          break;
        case 'terminating':
        case 'terminated':
          self.callSession = null;
          self.currentCallID = null;
          console.log('[sipml] - terminating -or- terminated');
          break;
        case 'm_permission_requested':
          console.log('[sipml] - mic permission requested');
          bean.fire(core, 'btsdk.sip.mic_permission_requested');
          break;
        case 'm_permission_accepted':
          console.log('[sipml] - mic permission accepted');

          bean.fire(core, 'btsdk.sip.mic_permission_accepted');
          break;
        case 'm_permission_refused':
          console.log('[sipml] - mic permission denied');

          self.currentState = 'mic_refused';
          bean.fire(core, 'btsdk.sip.mic_permission_refused');
          break;
        default:
          console.log('[sipml] - unknown stack event type',e.type);
      }
    },

    sessionEventListener: function (e) {
      var self = this;
      // console.log('[sipml] - session event',e);

      switch(e.type) {
        case 'connected':
          if(e.session == self.registerSession) {
            bean.fire(core, 'btsdk.sip.registered', { session: e.session });
          } else if(e.session == self.callSession) {
            bean.fire(core, 'btsdk.sip.call_connected', { session: e.session });
          } else {
            console.log('[sipml] - unknown session connected (register or call?)');
          }
          break;
        case 'connecting':
        case 'sent_request':
        case 'terminating':
          // no care, for now
          break;
        case 'terminated':
          console.log('[sipml] - self.currentState',self.currentState);

          switch(self.currentState) {
            case 'initialized':
              // termination happened during registration (likely invalid creds)
              bean.fire(core, 'btsdk.sip.registration_failure');
              break;
            case 'call_start':
              // termination happened while in-call, normal hangup
              self.callSession = null;
              self.currentCallID = null;
              bean.fire(core, 'btsdk.sip.hangup');
              break;
            case 'registered':
              console.log('[sipml] - session terminated, description:', e.description, e);
              // no action, for now
              break;
            default:
              console.log('[sipml] - session terminated (unknown reason, likely `destroy`)',e);
          }
          break;
        case 'transport_error':
          console.log('[sipml] - transport_error',e);
          self.clearSosKeepalive();
          break;
        case 'i_ao_request':
          console.log('[sipml] - i_ao_request',e);
          break;
        case 'm_early_media':
          console.log('[sipml] - m_early_media',e);
          self.currentCallID = e.o_event.o_message.o_hdr_Call_ID.s_value;
          break;
        case 'm_stream_audio_local_added':
          console.log('[sipml] - m_stream_audio_local_added',e);
          break;
        case 'm_stream_audio_remote_added':
          console.log('[sipml] - m_stream_audio_remote_added',e);
          break;
        default:
          console.log('[sipml] - unknown session event type',e.type,e);
      }
    },

    engineListeners: function () {
      var self = this;

      bean.on(core, 'btsdk.sip.initialized', function (e) {
        self.currentState = 'initialized';
      });

      bean.on(core, 'btsdk.sip.registered', function (e) {
        self.currentState = 'registered';
      });

      bean.on(core, 'btsdk.sip.call_start', function (e) {
        self.currentState = 'call_start';
      });
    },

    muteAction: function (enabled) {
      // mute isn't "officially" supported
      // see: http://code.google.com/p/sipml5/issues/detail?id=67

      if(this.callSession == null) {
        console.info('[sipml] - mute: no callSession');
        return;
      }

      if(this.callSession.o_session == null) {
        console.info('[sipml] - mute: no session');
        return;
      }

      if(this.callSession.o_session.o_stream_local == null) {
        console.info('[sipml] - mute: no local stream');
        return;
      }

      if(this.callSession.o_session.o_stream_local.getAudioTracks().length == 0) {
        console.info('[sipml] - mute: no audio tracks');
        return;
      }

      for(var nTrack = 0; nTrack < this.callSession.o_session.o_stream_local.getAudioTracks().length ; nTrack++) {
        console.info('[sipml] - mute: setting track.enabled to', enabled);
        this.callSession.o_session.o_stream_local.getAudioTracks()[nTrack].enabled = enabled;
      }
    },

    startSosKeepalive: function () {
      if(!config.sos) return;

      var self = this;

      self.sosKeepaliveID = setInterval(function () {
        self.msgSession = self.sipStack.newSession('message', {
          events_listener: {
            events: '*',
            listener: function (e) {
              // console.log('[sipml] - msg event',e);
            }
          }
        });

        self.msgSession.send(config.address + '.sos.keepalive', 'sos.keepalive', 'text/plain;charset=utf-8');
      }, self.sosKeepaliveInterval);
    },

    clearSosKeepalive: function () {
      var self = this;

      if(self.sosKeepaliveID) {
        clearInterval(self.sosKeepaliveID);
        self.sosKeepaliveID = null;
      }
    },

    /***********************************************
     *
     * BT SIP Interface Functions
     *
     ***********************************************/

    init: function (config) {
      console.log('[sipml] - initializing...', config);

      this.engineListeners();

      var fnReady = function (e) {
        console.log('[sipml] - init fnReady', e);
      };

      var fnError = function (e) {
        console.error('[sipml] - init fnError', e);
        bean.fire(core, 'btsdk.sip.error', e);
      };

      sipml.init(fnReady, fnError);

      this.createSipStack(config);
    },

    createSipStack: function (config) {
      var self = this;

      self.sipStack = new sipml.Stack({
        realm: config.realm,
        impi: config.impi,
        impu: config.impu,
        password: config.pass,
        display_name: config.impi,
        websocket_proxy_url: config.websocket,
        outbound_proxy_url: null,
        ice_servers: null,
        enable_rtcweb_breaker: true,
        enable_media_stream_cache: true,
        events_listener: { events: '*', listener: self.stackEventListener.bind(self) },
        enable_early_ims: true,

        sip_headers: [
          { name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.2013.04.26' },
          { name: 'Organization', value: 'Doubango Telecom' }
        ]
      });

      if(self.sipStack.start() !== 0) {
        console.error("[sipml] - start returned non-zero exit");
        bean.fire(core, 'btsdk.sip.error', {});
      }
    },

    updateConfig: function (key, value) {
      var self   = this,
          config = {};

      config[key] = value;
      self.sipStack.setConfiguration(config);
    },

    muteMic: function () {
      this.muteAction(false);
    },

    unmuteMic: function () {
      this.muteAction(true);
    },

    register: function () {
      var self = this;

      console.log('[sipml] - registering...');

      var opts = {
        expires: 200,
        events_listener: { events: '*', listener: self.sessionEventListener.bind(self) }, // optional: '*' means all events
        sip_caps: [
          { name: '+g.oma.sip-im', value: null },
          //{ name: '+sip.ice' }, // rfc5768: FIXME doesn't work with Polycom TelePresence
          { name: '+audio', value: null },
          { name: 'language', value: '\"en,fr\"' }
        ]
      };

      // SOS cannot register more than once
      if(config.sos) {
        delete opts.expires;
      }

      self.registerSession = self.sipStack.newSession('register', opts);
      self.registerSession.register();
    },

    call: function (number) {
      var self = this;

      if(!self.sipStack) {
        console.error('[sipml] - sip stack not initialized');
        return;
      }

      if(!self.callSession) {
        console.error('not compatible with new <video> tags');

        // create a new call session
        self.callSession = self.sipStack.newSession('call-audio', {
          audio_remote: document.getElementById('audio-remote'),
          video_local: null,
          video_remote: null,
          events_listener: { events: '*', listener: self.sessionEventListener.bind(self) },
          sip_caps: [
            { name: '+g.oma.sip-im' },
            // { name: '+sip.ice' },
            { name: 'language', value: '\"en,fr\"' }
          ]
        });

        if(self.callSession === null) {
          console.error('[sipml] - failed to start call session');
          return;
        }
      }

      if(self.callSession.call(number) != 0) {
        self.callSession = null;
        self.currentCallID = null;
        console.error("[sipml] - failed to make call");
        return;
      }

      bean.fire(core, 'btsdk.sip.call_start');
    },

    hangup: function () {
      var self = this;

      if(!self.callSession) {
        console.log('[sipml] - no call session to hangup');
        return;
      }

      self.callSession.hangup();
    },

    destroy: function () {
      var self = this;

      self.sipStack.stop();
      self.clearSosKeepalive();

      self.registerSession = null;
      self.callSession = null;
      self.currentCallID = null;
      self.currentState = null;

      bean.fire(core, 'btsdk.sip.destroy');
    },

  };
});