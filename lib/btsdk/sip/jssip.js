define([
  'btsdk/core',
  'btsdk/config',
  'hark',
  'jssip',
  'bean'
], function (core, config, hark, jssip, bean) {
  'use strict';

  return {
    name: 'jssip',

    sipStack: null,

    registerSession: null,

    callSession: null,

    currentState: null,

    currentCallID: null,

    // decibels, -100 = silence, 0 = loudest
    micVolume: -100,

    attachStackListeners: function () {
      var self = this;

      self.sipStack.on('connecting', function (e) {
        console.log('[jssip] - connecting');
      });

      self.sipStack.on('connected', function (e) {
        bean.fire(core, 'btsdk.sip.initialized');
        console.log('[jssip] - connected');
      });

      self.sipStack.on('disconnected', function (e) {
        console.log('[jssip] - disconnected');
        bean.fire(core, 'btsdk.sip.disconnected');
      });

      self.sipStack.on('registered', function (e) {
        console.log('[jssip] - registered');

        bean.fire(core, 'btsdk.sip.registered');
      });

      self.sipStack.on('unregistered', function (e) {
        console.log('[jssip] - registered');
      });

      self.sipStack.on('registrationFailed', function (e) {
        console.log('[jssip] - registrationFailed');

        bean.fire(core, 'btsdk.sip.registration_failure');
      });

      self.sipStack.on('newRTCSession', function (e) {
        console.log('[jssip] - newRTCSession');

        var rtcsession = e,
            request = e.request,
            header  = null;

        if (e.session.direction == 'incoming') {
          self.callSession = e.session;
          header = 'X-Btinstigatorid';
        }
        else {
          // outbound call, use sip call-id
          // self.callSession is set in call() method for outbound calls
          header = 'Call-ID';
        }

        try {
          if (request.hasHeader(header)) {
            self.currentCallID = request.getHeader(header);
          }
          else {
            console.log('[jssip] - header',header,'not found');
            self.currentCallID = null;
          }
        }
        catch (err) {
          console.log('[jssip] - exception while parsing header',header,err);
          self.currentCallID = null;
        }

        if (e.session.direction == 'incoming') {
          bean.fire(core, 'btsdk.sip.ringing', self.currentCallID);

          e.session.on('failed', function (e) {
            console.log('[jssip] - call session failed',e);
            var callID = self.currentCallID;
            self.reactToHangup();
            bean.fire(core, 'btsdk.sip.hangup', {callID: callID, cause: e.cause});
          });
        }

        e.session.on('getusermediafailed', function (e) {
          bean.fire(core, 'btsdk.sip.mic_permission_refused');
        });

        e.session.on('connecting', function (e) {
          if (rtcsession.session.connection) {
            var listener, timerStart, hostCt = 0, srflxCt = 0;

            rtcsession.session.connection.addEventListener('icecandidate', listener = function(event) {
              if (!timerStart) timerStart = performance.now();

              var candidate = event.candidate;
              bean.fire(core, 'btsdk.sip.icecandidate', candidate);

              if (candidate) {
                if (!!candidate.candidate.match(/typ srflx/)) {
                  srflxCt++;
                }
                else {
                  hostCt++;
                }
                return;
              }

              if (timerStart) {
                var ms = Math.round(performance.now() - timerStart);

                if (core.api.int.ws && core.api.int.ws.readyState === 1) {
                  var authUser = core.user.authUser ? core.user.authUser : null;
                  core.api.int.send({
                      event: 'telemetry',
                      data: {
                        type: 'icegathering',
                        ms: ms,
                        candHostCt: hostCt,
                        candSrflxCt: srflxCt,
                        userAgent: window.navigator.userAgent,
                        customerID: authUser ? authUser.customerID : null,
                        userID: authUser ? authUser.userID : null,
                        siteID: authUser ? authUser.siteID : null,
                      }
                  });
                }
                else {
                  console.log('[jssip] - cannot send icegathering telemetry (no api-int ws). gathering time was: ' + ms + 'ms');
                }
              }

              rtcsession.session.connection.removeEventListener('icecandidate', listener);
            });
          }

          bean.fire(core, 'btsdk.sip.mic_permission_accepted');
        });

      });
    },

    attachSessionListeners: function () {
      var self = this;
    },

    init: function (config) {
      var self = this;

      self.config = config;

      console.log('[jssip] - initializing...', config);

      jssip.debug.enable('*');

      self.sipStack = new jssip.UA({
        sockets  : [ new jssip.WebSocketInterface(config.websocket) ],
        uri: config.impu,
        password: config.pass,
        authorization_user: config.impi,
        register_expires: 60,
        register: false
      });

      self.attachStackListeners();
      self.sipStack.start();
    },

    updateConfig: function (key, value) {
      if (key !== 'password') {
        throw "[jssip] - only `password` config option can be updated";
      }

      this.sipStack.terminateSessions();
      this.sipStack.stop();
      this.sipStack = null;

      this.sipStack = new jssip.UA({
        sockets  : [ new jssip.WebSocketInterface(this.config.websocket) ],
        uri: this.config.impu,
        password: value,
        authorization_user: this.config.impi,
        register_expires: 60,
        register: false
      });

      this.attachStackListeners();
      this.sipStack.start();
    },

    muteMic: function () {
      if (!this.callSession) {
        console.log('[jssip] - no call session to mute');
        return;
      }

      this.callSession.mute({ audio: true, video: true });
    },

    unmuteMic: function () {

      if (!this.callSession) {
        console.log('[jssip] - no call session to mute');
        return;
      }

      this.callSession.unmute({ audio: true, video: true });
    },

    register: function () {
      var self = this;

      console.log('[jssip] - registering...');

      self.sipStack.register();
    },

    call: function (number) {
      var self = this,
          firedRingback = false;

      console.log('[jssip] - call', number);

      var eventHandlers = {
        progress: function (e) {
          console.log('[jssip] - call progress',e);

          if (e.response.status_code == 180) {
            console.log('[jssip] - call progress, receiving ringback');

            if (!firedRingback) {
              bean.fire(core, 'btsdk.sip.ringback', self.currentCallID);
              firedRingback = true;

              var localStream = self.callSession.connection.getLocalStreams()[0];
              self.startMicMeter(localStream);
            }
            else {
              console.log('[jssip] - call progress, skipping firing ringback event, already fired');
            }
          }
        },

        failed: function (e) {
          console.log('[jssip] - call session failed',e);

          var callID = self.currentCallID;
          self.reactToHangup();
          bean.fire(core, 'btsdk.sip.hangup', {callID: callID, cause:  e.cause});
        },

        confirmed: function (e) {
          console.log('[jssip] - call confirmed',e);

          if (config.video) {
            self.attachLocalStream();
          }

          bean.fire(core, 'btsdk.sip.call_connected', self.currentCallID);
        },

        ended: function (e) {
          console.log('[jssip] - outbound call ended',e);

          var callID = self.currentCallID;
          self.reactToHangup();
          bean.fire(core, 'btsdk.sip.hangup', {callID: callID, cause: e.cause});
        }
      };

      var options = {
        eventHandlers: eventHandlers,
        mediaConstraints: { audio: true, video: config.video },
        pcConfig: {
          rtcpMuxPolicy: 'negotiate',
          iceServers: [
            { urls: config.iceServers }
          ]
        }
      };

      self.callSession = self.sipStack.call(number, options);

      self.callSession.connection.ontrack = self.attachRemoteStream.bind(this);
    },

    answer: function () {
      var self = this;

      if (!self.callSession) {
        console.log('[jssip] - no callSession');
        return;
      }

      if (self.callSession.direction == 'outgoing') {
        console.log('[jssip] - callSession is not an incoming call');
        return;
      }

      self.callSession.on('accepted', function (e) {
        console.log('[jssip] - inbound call accepted',e);

        var localStreams = self.callSession.connection.getLocalStreams();

        if (localStreams.length > 0) {
          var streamLocal = document.getElementById(config.selectors.streamLocal);

          if (streamLocal) {
            streamLocal.srcObject = localStreams[0];
            streamLocal.volume = 0;
            console.log('[jssip] - local stream attached');

            self.startMicMeter(localStreams[0]);
          }
          else {
            console.log('[jssip] - no local stream element found');
          }
        }

        bean.fire(core, 'btsdk.sip.call_connected', self.currentCallID);
      });

      self.callSession.on('ended', function (e) {
        console.log('[jssip] - inbound call ended',e);

        var callID = self.currentCallID;
        self.reactToHangup();
        bean.fire(core, 'btsdk.sip.hangup', {callID: callID, cause: e.cause});
      });

      var options = {
        mediaConstraints: { audio: true, video: config.video },
        pcConfig: {
          rtcpMuxPolicy: 'negotiate',
          iceServers: [
            { urls: config.iceServers }
          ]
        },
        rtcOfferConstraints: {
          offerToReceiveAudio: 1,
          offerToReceiveVideo: 1
        },
      };

      self.callSession.answer(options);

      self.callSession.connection.ontrack = self.attachRemoteStream.bind(this);
    },

    hangup: function () {
      console.log('[jssip] - hangup');
      this.sipStack.terminateSessions();
    },

    destroy: function () {
      console.log('[jssip] - destroy');
      this.sipStack.terminateSessions();
      this.sipStack.stop();

      this.registerSession = null;
      this.callSession     = null;
      this.currentCallID   = null;
      this.currentState    = null;

      bean.fire(core, 'btsdk.sip.destroy');
    },

    reactToHangup: function () {
      this.resetVolume();

      this.currentCallID = null;
      this.callSession   = null;

      var local  = document.getElementById(config.selectors.streamLocal),
          remote = document.getElementById(config.selectors.streamRemote);

      if (local) {
        local.srcObject = null;
      }

      if (remote) {
        remote.srcObject = null;
      }
    },

    attachRemoteStream: function (event) {
      console.log('[jssip] - attachRemoteStream');

      var streamRemote = document.getElementById(config.selectors.streamRemote);

      if (!streamRemote) {
        console.log('[jssip] - no remote stream element found');
        return;
      }

      if (!event || !event.streams || event.streams.length === 0) {
        console.log('[jssip] - no streams found on event');
        return;
      }

      streamRemote.srcObject = event.streams[0];
    },

    attachLocalStream: function (event) {
      console.log('[jssip] - attachLocalStream');

      var streamLocal = document.getElementById(config.selectors.streamLocal);

      if (!streamLocal) {
        console.log('[jssip] - no local stream element found');
        return;
      }

      streamLocal.srcObject = this.callSession.connection.getLocalStreams()[0];
      streamLocal.volume = 0; // local is video-only
    },

    startMicMeter: function (stream) {
      if (!config.micStats) return;
      if (!stream) return console.log('[jssip] - invalid stream object passed to meter', stream);

      var self = this;

      this.resetVolume();
      this.meter = hark(stream);

      this.meter.on('volume_change', function (volume, threshold) {
        self.micVolume = volume;
      });

      console.log('[jssip] - mic meter connected to stream');
    },

    resetVolume: function () {
      if (this.meter) {
        this.meter.stop();
        this.meter = null;
      }

      this.micVolume = -100;
    },

    micStats: function () {
      return {
        volume: this.micVolume
      };
    },

  };
});