define([
  'btsdk/core',
  'sipjs',
  'bean'
], function(core, sipjs, bean) {
  'use strict';

  return {
    name: 'sipjs',

    sipStack: null,

    registerSession: null,

    callSession: null,

    currentState: null,

    attachStackListeners: function() {
      var self = this;

      self.sipStack.on('connected', function(e) {
        bean.fire(core, 'btsdk.sip.initialized');
      });

      self.sipStack.on('disconnected', function(e) {
        console.log('[sipjs] - disconnected');
        //@todo: fire event for ws disconnect
      });

      self.sipStack.on('registered', function(e) {
        bean.fire(core, 'btsdk.sip.registered');
      });

      self.sipStack.on('registrationFailed', function(e) {
        bean.fire(core, 'btsdk.sip.registration_failure');
      });
    },

    attachSessionListeners: function() {
      var self = this;

      self.callSession.on('accepted', function(e) {
        bean.fire(core, 'btsdk.sip.call_start');
      });

      self.callSession.on('bye', function(e) {
        bean.fire(core, 'btsdk.sip.hangup');
      });
    },

    init: function(config) {
      var self = this;

      console.log('[sipjs] - initializing...', config);

      self.sipStack = new sipjs.UA({
        wsServers: config.websocket,
        uri: config.impu,
        authorizationUser: config.impi,
        password: config.pass,
        register: false
      });

      self.attachStackListeners();
      self.sipStack.start();
    },

    register: function() {
      var self = this;

      console.log('[sipjs] - registering...');

      self.sipStack.register();
    },

    call: function(number) {
      var self = this;

      if(!self.sipStack) {
        console.error('[sipjs] - sip stack not initialized');
        return;
      }

      self.callSession = self.sipStack.invite(number, {
        media: {
          constraints: {
            audio: true,
            video: false
          },
          // stream: self.gainController.outputStream,
          render: {
            remote: {
              audio: document.getElementById('audio-remote')
            }
          }
        }
      });

      self.attachSessionListeners();
    },

    hangup: function() {
      var self = this;
      console.log('[sipjs] - hangup');

      if(!self.callSession) {
        console.log('[sipjs] - no session to hangup');
        return;
      }

      self.callSession.bye();
    },

    destroy: function() {
      var self = this;

      this.sipStack.stop();

      this.registerSession = null;
      this.callSession = null;
      this.currentState = null;
    },

    muteMic: function() {
      console.log('[sipjs] - this.gainController',this.gainController);
      this.gainController.off();
    },

  };
});