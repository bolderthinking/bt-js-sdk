define([], function () {
  'use strict';

  return {
    engine: 'jssip',
    env: 'humach.com',
    cdn: 'code.humach.com/code',
    video: false,
    micStats: false,
    api: {
      int: {
        heartbeatSecs: 30,
      },
    },
    iceServers: ['stun:stun.l.google.com:19302'],
    selectors: {
      streamRemote: 'btsdk-stream-remote',
      streamLocal: 'btsdk-stream-local'
    }
  };

});