define([
  'btsdk/core',
  'btsdk/config',
  'btsdk/api/abstract',
  'bean',
  'underscore',
  'reconnecting-ws',
], function (core, config, abstract, bean, _, ReconnectingWebSocket) {
  'use strict';

  return _.extend({

    ws: null,

    wsParams: null,

    ref: 1,

    /**
     * Connects the WebSocket.
     *
     * @return {void}
     */
    wsConnect: function () {
      this.wsParams = { format: 'json', proxyToken: this.proxyToken() };

      this.ws = new ReconnectingWebSocket(this.baseUrl('int') + '/authSession', null, {
        debug: false,
        reconnectInterval: 2000,
        timeoutInterval: 30000,
        params: this.wsParams
      });

      this.ws.onopen    = this.onOpen.bind(this);
      this.ws.onclose   = this.onClose.bind(this);
      this.ws.onerror   = this.onError.bind(this);
      this.ws.onmessage = this.onMessage.bind(this);

      bean.on(core, 'btsdk.user.token_renew', this.onTokenRenew.bind(this));
    },

    wsClose: function () {
      if(this.ws === null) {
        return console.log('[int] - wss not open')
      }

      this.ws.close();
    },

    /**
     * Connects the WebSocket.
     *
     * @return {void}
     */
    onOpen: function () {
      console.log('[int] - ws open');

      if (config.api && config.api.int && config.api.int.heartbeatSecs && parseInt(config.api.int.heartbeatSecs) > 0) {
        var self = this;
        this.heartbeat = window.setInterval(function () { self.ping() }, parseInt(config.api.int.heartbeatSecs) * 1000);
      }

      // token renews may complete just before socket connection
      // always updating upon WS open ensures it has the most up-to-date token
      this.updateToken(this.wsParams.proxyToken);

      bean.fire(core, 'btsdk.int.ws_open');
    },

    /**
     * WebSocket message event.
     *
     * @return {void}
     */
    onMessage: function (evt) {
      try {
        var json = JSON.parse(evt.data);
        bean.fire(core, 'btsdk.int.ws_message_receive', evt.data);
      } catch(err) {
        console.error('[int] - failed to parse received message "' + evt.data + '"');
        return;
      }

      this.receive(json);
    },

    /**
     * WebSocket error event.
     *
     * @return {void}
     */
    onError: function (evt) {
      console.error('[int] - wss error', evt);

      bean.fire(core, 'btsdk.int.ws_error', {
        error: 'WebSocket Error',
        event: evt
      });
    },

    /**
     * WebSocket closed event.
     *
     * @return {void}
     */
    onClose: function (evt) {
      console.log('[int] - wss close');
      bean.fire(core, 'btsdk.int.ws_close');

      if (this.heartbeat) {
        window.clearInterval(this.heartbeat);
        this.heartbeat = null;
      }
    },

    onTokenRenew: function (data) {
      this.wsParams.proxyToken = data.token.proxyToken;
      this.updateToken(data.token.proxyToken);
    },

    /**
     * Checks that WebSocket connection exists and is in the OPEN state.
     *
     * @param  {bool} triggerError If an error should be triggered when the WS is not connected.
     * @return {bool}
     */
    isConnected: function (triggerError) {
      if(!this.ws || this.ws.readyState !== 1) {
        if(triggerError) {
          bean.fire(core, 'btsdk.api.int.error', {
            error: 'WebSocket not connected'
          });
        }
        return false;
      }
      return true;
    },

    /**
     * Sends a JSON message over the WebSocket.
     *
     * @param  {string} data
     * @return {void}
     */
    send: function (data) {
      if (!this.isConnected()) {
        console.log('[int] - cannot send message, wss not connected. data', data);
        return;
      }

      data.ref = String(this.ref);
      this.ws.send(JSON.stringify(data));
      bean.fire(core, 'btsdk.int.ws_message_send', JSON.stringify(data));
      this.ref++;
    },

    /**
     * Handles decoded JSON message objects the WebSocket receives.
     *
     * @param  {object} data
     * @return {void}
     */
    receive: function (data) {
      if(typeof data.event === 'undefined') {
        console.log('[int] - received unknown message',data);
        return;
      }

      if(data.error && data.error !== 'Invalid message') {
        bean.fire(core, 'btsdk.api.int.error', data);
        return;
      }

      switch(data.event) {
        case 'subscribe':
          if(data.data.userID) {
            bean.fire(core, 'btsdk.user.subscribe', data.data);
          }
          else if(data.data.callID) {
            bean.fire(core, 'btsdk.call.subscribe', data.data);
          }
          break;
        case 'unsubscribe':
          if(data.data.userID) {
            bean.fire(core, 'btsdk.user.unsubscribe', data.data);
          }
          else if(data.data.callID) {
            bean.fire(core, 'btsdk.call.unsubscribe', data.data);
          }
          break;
        case 'userStatus':
          bean.fire(core, 'btsdk.user.user_status', data.data);
          break;
        case 'userReset':
          bean.fire(core, 'btsdk.user.user_reset', data.data);
          break;
        case 'dial':
          bean.fire(core, 'btsdk.call.dial', data.data);
          break;
        case 'hold':
          bean.fire(core, 'btsdk.call.hold', data.data);
          break;
        case 'unhold':
          bean.fire(core, 'btsdk.call.unhold', data.data);
          break;
        case 'mute':
          bean.fire(core, 'btsdk.call.mute', data.data);
          break;
        case 'unmute':
          bean.fire(core, 'btsdk.call.unmute', data.data);
          break;
        case 'pauseRecording':
          bean.fire(core, 'btsdk.call.pause_recording', data.data);
          break;
        case 'resumeRecording':
          bean.fire(core, 'btsdk.call.resume_recording', data.data);
          break;
        case 'hangup':
          bean.fire(core, 'btsdk.call.hangup', data.data);
          break;
        case 'blindTransfer':
          bean.fire(core, 'btsdk.call.blind_transfer', data.data);
          break;
        case 'join':
          bean.fire(core, 'btsdk.call.join', data.data);
          break;
        case 'split':
          bean.fire(core, 'btsdk.call.split', data.data);
          break;
        case 'dtmf':
          bean.fire(core, 'btsdk.call.dtmf', data.data);
          break;
        case 'callEvent':
          bean.fire(core, 'btsdk.call.call_event', data.data);
          break;
        case 'modifyLobby':
          bean.fire(core, 'btsdk.lobby.modify_lobby', data.data);
          break;
        case 'stateLogin':
          bean.fire(core, 'btsdk.user.state_login', data.data);
          break;
        case 'stateLogout':
          bean.fire(core, 'btsdk.user.state_logout', data.data);
          break;
        case 'mcbLogin':
          bean.fire(core, 'btsdk.user.mcb_login', data.data);
          break;
        case 'mcbLogout':
          bean.fire(core, 'btsdk.user.mcb_logout', data.data);
          break;
        case 'mcbAction':
          bean.fire(core, 'btsdk.user.mcb_action', data.data);
          break;
        case 'stateChange':
          bean.fire(core, 'btsdk.user.state_change', data.data);
          break;
        case 'availableStates':
          bean.fire(core, 'btsdk.user.available_states', data.data);
          break;
        case 'ping':
        case 'pong':
        case 'updateToken':
        case 'telemetry':
          break;
        default:
          console.log('[int] - unknown event type', data);
      }
    },

    ping: function () {
      this.send({
        event: 'ping',
        data:  {}
      });
    },

    /**
     * Updates the socket's auth proxy token.
     * @todo: update to JSON message once server supports it
     *
     * @param  {string} proxyToken The proxy token string.
     * @return {void}
     */
    updateToken: function (proxyToken) {
      if (!this.isConnected(false)) {
        console.log('[int] - cannot renew token, wss not connected');
        return;
      }

      this.send({
        event: 'updateToken',
        data: {
          proxyToken: proxyToken
        }
      });
    },

    /////////////////////////////////////////////////////////////////////
    ///    User events
    /////////////////////////////////////////////////////////////////////

    /**
     * Subscribes to events for a call and/or user.
     *
     * @param  {object} opts
     * @param  {mixed}  opts.userID   A string or array of userIDs.
     * @param  {mixed}  opts.callID   A string or array of callIDs.
     * @return {void}
     */
    subscribe: function (opts) {
      this.send({
        event: 'subscribe',
        data: opts
      });
    },

    /**
     * Unsubscribes to events for a call and/or user.
     *
     * @param  {object} opts
     * @param  {mixed}  opts.userID   A string or array of userIDs.
     * @param  {mixed}  opts.callID   A string or array of callIDs.
     * @return {void}
     */
    unsubscribe: function (opts) {
      this.send({
        event: 'unsubscribe',
        data: opts
      });
    },

    /**
     * Gets a user's current status.
     * This includes information about their current state, and ongoing calls.
     *
     * @param  {string} userID
     * @return {void}
     */
    userStatus: function (userID) {
      this.send({
        event: 'userStatus',
        data: {
          userID: userID
        }
      });
    },

    /**
     * Deletes a user process.
     * Should NOT be used for anything other than debug scenarios.
     *
     * @param  {int} userID
     * @return {void}
     */
    userReset: function (userID) {
      this.send({
        event: 'userReset',
        data: {
          userID: userID
        }
      });
    },

    /**
     * Logs a user into state management.
     *
     * @param  {int} userID
     * @return {void}
     */
    stateLogin: function (opts) {
      this.send({
        event: 'stateLogin',
        data: {
          userID: opts.userID,
          endpointAddressID: opts.endpointAddressID
        }
      });
    },

    /**
     * Logs a user out of state management.
     *
     * @param  {int} userID
     * @return {void}
     */
    stateLogout: function (userID) {
      this.send({
        event: 'stateLogout',
        data: {
          userID: userID
        }
      });
    },

    /**
     * Gets a user's possible states.
     *
     * @param  {int} userID
     * @return {void}
     */
    availableStates: function (userID) {
      this.send({
        event: 'availableStates',
        data: {
          userID: userID
        }
      });
    },

    /**
     * Changes a user's state + sub-state.
     *
     * @param  {object} opts
     * @return {void}
     */
    stateChange: function (opts) {
      this.send({
        event: 'stateChange',
        data: {
          userID: opts.userID,
          stateID: String(opts.stateID),
          subStateID: String(opts.subStateID)
        }
      });
    },

    /////////////////////////////////////////////////////////////////////
    ///    MCB (monitor/coach/barge) events
    /////////////////////////////////////////////////////////////////////

    /**
     * Logs a user in for MCB.
     *
     * @param  {object} opts
     * @return {void}
     */
    mcbLogin: function (opts) {
      this.send({
        event: 'mcbLogin',
        data: {
          userID: opts.userID,
          endpointAddressID: opts.endpointAddressID
        }
      });
    },

    /**
     * Logs a user out from MCB.
     *
     * @param  {object} opts
     * @return {void}
     */
    mcbLogout: function (opts)  {
      this.send({
        event: 'mcbLogout',
        data: {
          userID: opts.userID
        }
      });
    },

    /**
     * Executes an MCB action for the given user against the target user.
     *
     * @param  {object} opts
     * @return {void}
     */
    mcbAction: function (opts) {
      this.send({
        event: 'mcbAction',
        data: {
          userID: opts.userID,
          targetUserID: opts.targetUserID,
          action: opts.action
        }
      });
    },

    /////////////////////////////////////////////////////////////////////
    ///    EPA events
    /////////////////////////////////////////////////////////////////////

    /**
     * Gets an endpoint address' current status.
     * This includes information about its state and connection method.
     *
     * @param  {string} endpointAddress
     * @return {void}
     */
    endpointAddressStatus: function (epa) {
      this.send({
        event: 'endpointAddressStatus',
        data: {
          endpointAddressID: epa
        }
      });
    },

    /////////////////////////////////////////////////////////////////////
    ///    Call events
    /////////////////////////////////////////////////////////////////////

    /**
     * Requests a dial on an endpoint address.
     *
     * @param  {object} opts
     * @return {void}
     */
    dial: function (opts) {
      this.send({
        event: 'dial',
        data: {
          endpointAddressID: opts.endpointAddressID,
          number: opts.number,
          outboundProfileID: opts.outboundProfileID,
          lobbyID: opts.lobbyID,
          queueID: opts.queueID
        }
      });
    },

    /**
     * Requests a call be placed on hold.
     *
     * @param  {object} opts
     * @return {void}
     */
    hold: function (callID) {
      this.send({
        event: 'hold',
        data: {
          callID: callID
        }
      });
    },

    /**
     * Requests a call be taken off hold.
     *
     * @param  {object} opts
     * @return {void}
     */
    unhold: function (callID) {
      this.send({
        event: 'unhold',
        data: {
          callID: callID
        }
      });
    },

    /**
     * Requests a call to be muted
     *
     * @param  {object} opts
     * @return {void}
     */
    mute: function (callID) {
      this.send({
        event: 'mute',
        data: {
          callID: callID
        }
      });
    },

    /**
     * Requests a call to be taken off mute
     *
     * @param  {object} opts
     * @return {void}
     */
    unmute: function (callID) {
      this.send({
        event: 'unmute',
        data: {
          callID: callID
        }
      });
    },

    /**
     * Requests a call's recording to be paused
     *
     * @param  {object} opts
     * @return {void}
     */
    pauseRecording: function (opts) {
      this.send({
        event: 'pauseRecording',
        data: {
          lobbyID: opts.lobbyID
        }
      });
    },

    /**
     * Requests a call's recording to be resumed
     *
     * @param  {object} opts
     * @return {void}
     */
    resumeRecording: function (opts) {
      this.send({
        event: 'resumeRecording',
        data: {
          lobbyID: opts.lobbyID
        }
      });
    },

    /**
     * Sends a DTMF digit press
     *
     * @param  {object} opts
     * @return {void}
     */
    dtmf: function (opts) {
      this.send({
        event: 'dtmf',
        data: {
          callID: opts.callID,
          digit: opts.digit
        }
      });
    },

    /**
     * Updates a lobby with new parameters
     *
     * @param  {object} opts
     * @return {void}
     */
    modifyLobby: function (opts) {
      this.send({
        event: 'modifyLobby',
        data: {
          lobbyID: opts.lobbyID,
          persistLobby: opts.persistLobby,
          releaseLobbyControl: opts.releaseLobbyControl
        }
      });
    },

    /**
     * Requests a call be hung up.
     *
     * @param  {object} opts
     * @return {void}
     */
    hangup: function (opts) {
      this.send({
        event: 'hangup',
        data: {
          callID: opts.callID,
          lobbyID: opts.lobbyID,
          persistLobby: typeof opts.persistLobby === 'undefined' ? false : opts.persistLobby,
          releaseLobbyControl: typeof opts.releaseLobbyControl === 'undefined' ? false : opts.releaseLobbyControl
        }
      });
    },

    /**
     * Accomplishes a "split" of the given calls,
     * then lobby-dials the given number.
     *
     * @param  {object} opts
     * @return {void}
     */
    blindTransfer: function (opts) {
      this.send({
        event: 'blindTransfer',
        data: {
          number: opts.number,
          callID: opts.callID
        }
      });
    },

    /**
     * Requests one or more calls are "joined" into the given lobby.
     *
     * @param  {object} opts
     * @return {void}
     */
    join: function (opts) {
      this.send({
        event: 'join',
        data: {
          callID: opts.callID,
          lobbyID: opts.lobbyID
        }
      });
    },

    /**
     * Requests one or more calls are "split" from their current lobby.
     *
     * @param  {object} opts
     * @return {void}
     */
    split: function (opts) {
      this.send({
        event: 'split',
        data: {
          callID: opts.callID,
          releaseLobbyControl: typeof opts.releaseLobbyControl === 'undefined' ? false : opts.releaseLobbyControl
        }
      });
    },

  }, abstract);

});