define([
  'btsdk/core',
  'btsdk/api/abstract',
  'reqwest',
  'underscore'
], function (core, abstract, reqwest, _) {
  'use strict';

  return _.extend({

    /**
     * Fetches call attached data for the given call id.
     *
     * @param  {string} cid
     * @param  {string} epa
     * @param  {string} pass
     * @return {promise}
     */
    fetchAllCallAttachedData: function (cid, epa, pass) {
      console.log('[api] - fetching all call-attached-data',{cid: cid, epa: epa, pass: pass});

      if(!cid) {
        console.log('[api] - no call id, cannot fetch data');
        return null;
      }

      return reqwest({
        url: this.baseUrl('edge') + '/rest/call/customer/' + cid,
        crossOrigin: true,
        method: 'get',
        type: 'json',
        headers: this.basicAuthHeaders(epa, pass),
        data: {
          ver: 'latest',
          format: 'json',
          key: 'all'
        }
      });
    },

    /**
     * Sets call attached data for the given call id.
     *
     * @param {string} cid
     * @param {string} epa
     * @param {string} pass
     * @param {string} key
     * @param {mixed} value
     * @return {promise}
     */
    setCallAttachedData: function (cid, epa, pass, key, value) {
      console.log('[api] - setting call-attached-data',{cid: cid, epa: epa, pass: pass, key: key});

      if(!cid) {
        console.log('[api] - no call id, cannot set data');
        return null;
      }

      return reqwest({
        url: this.baseUrl('edge') + '/rest/call/customer/' + cid,
        crossOrigin: true,
        method: 'put',
        type: 'json',
        contentType: 'application/json',
        headers: this.basicAuthHeaders(epa, pass),
        data: JSON.stringify({
          'ver': 'latest',
          'format': 'json',
          'key': key,
          'value': value
        })
      });
    },

    /**
     * Fetches system-public call attached data for the given call id.
     *
     * @param  {string} cid
     * @param  {string} epa
     * @param  {string} pass
     * @return {promise}
     */
    fetchAllSystemPublicData: function (cid, epa, pass) {
      console.log('[api] - fetching all system-public-data',{cid: cid, epa: epa, pass: pass});

      if(!cid) {
        console.log('[api] - no call id, cannot fetch data');
        return null;
      }

      return reqwest({
        url: this.baseUrl('edge') + '/rest/call/systemPublic/' + cid,
        crossOrigin: true,
        method: 'get',
        type: 'json',
        headers: this.basicAuthHeaders(epa, pass),
        data: {
          ver: 'latest',
          format: 'json',
          key: 'all'
        }
      });
    },

    //////////////////////////////////////////////////////////////////////////////////

    /**
     * Fetches call attached data for the given call id.
     *
     * @param  {string} cid
     * @param  {object} opts
     * @return {promise}
     */
    getCustomerCallData: function (cid, opts) {
      console.log('[api] - fetching customer CAD for call', cid, 'opts', opts);

      if(!cid) {
        console.log('[api] - no call id, cannot fetch data');
        return null;
      }

      return reqwest({
        url: this.baseUrl('edge') + '/rest/call/customer/' + cid,
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: _.extend({ver: 'latest', format: 'json', 'key': 'all'}, opts)
      });
    },

    /**
     * Sets call attached data for the given call id.
     *
     * @param {string} cid
     * @param {object} opts
     * @return {promise}
     */
    putCustomerCallData: function (cid, opts) {
      console.log('[api] - setting customer CAD for call', cid, 'opts', opts);

      if(!cid) {
        console.log('[api] - no call id, cannot set data');
        return null;
      }

      return reqwest({
        url: this.baseUrl('edge') + '/rest/call/customer/' + cid,
        crossOrigin: true,
        method: 'put',
        type: 'json',
        contentType: 'application/json',
        data: JSON.stringify(_.extend({ver: 'latest', format: 'json'}, opts))
      });
    },

    /**
     * Fetches system-public call attached data for the given call id.
     *
     * @param  {string} cid
     * @param  {object} opts
     * @return {promise}
     */
    getSystemPublicCallData: function (cid, opts) {
      console.log('[api] - fetching system-public CAD for call', cid, 'opts', opts);

      if(!cid) {
        console.log('[api] - no call id, cannot fetch data');
        return null;
      }

      return reqwest({
        url: this.baseUrl('edge') + '/rest/call/systemPublic/' + cid,
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: _.extend({ver: 'latest', format: 'json', 'key': 'all'}, opts)
      });
    },

  }, abstract);

});