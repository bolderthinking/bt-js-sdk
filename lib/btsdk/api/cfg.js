define([
  'require',
  'btsdk/core',
  'btsdk/api/abstract',
  'reqwest',
  'bean',
  'underscore'
], function (require, core, abstract, reqwest, bean, _) {
  'use strict';

  return _.extend({

    /**
     * POST /rest/user/{username}?method=proxyToken
     *
     * Creates a new proxyToken for a user.
     *
     * @param  {string} username
     * @param  {string} password
     * @return {promise}
     */
    createProxyToken: function (username, password) {
      return reqwest({
        url: this.baseUrl('cfg') + '/rest/user/' + username,
        crossOrigin: true,
        method: 'post',
        type: 'json',
        headers: this.basicAuthHeaders(username, password),
        data: { method: 'proxyToken', password: password, ver: 'latest', format: 'json' }
      });
    },

    /**
     * POST /rest/user?method=renewToken
     *
     * Renews a user's proxyToken before its `renewTS`.
     *
     * @return {promise}
     */
    renewProxyToken: function () {
      return reqwest({
        url: this.baseUrl('cfg') + '/rest/user',
        crossOrigin: true,
        method: 'post',
        type: 'json',
        data: { proxyToken: this.proxyToken(), method: 'renewToken', ver: 'latest', format: 'json' }
      });
    },

    /**
     * GET /rest/endpointList?userID={userID}
     *
     * Returns a list of a user's endpoints.
     *
     * @param  {string} userID
     * @return {promise}
     */
    endpointList: function (userID) {
      return reqwest({
        url: this.baseUrl('cfg') + '/rest/endpointList',
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: { userID: userID, proxyToken: this.proxyToken(), ver: 'latest', format: 'json' }
      });
    },

    speedDialList: function () {
      return reqwest({
        url: this.baseUrl('cfg') + '/rest/speedDialList',
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: { proxyToken: this.proxyToken(), ver: 'latest', format: 'json' }
      });
    },

    /**
     * GET /rest/user/{userID}?method=interfaceProfileList
     *
     * Returns a list of a user's interface profiles.
     *
     * @param  {string} userID
     * @return {promise}
     */
    interfaceProfileList: function (userID) {
      return reqwest({
        url: this.baseUrl('cfg') + '/rest/user/' + userID,
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: { method: 'interfaceProfileList', type: 'cloudphone', proxyToken: this.proxyToken(), ver: 'latest', format: 'json' }
      });
    },

    /**
     * GET /rest/queueList
     *
     * Returns a list of queues.
     *
     * @param  {object} An object of additional parameters to send (pageSize, lastReturnedKey, etc.)
     * @return {promise}
     */
    queueList: function (params) {
      return reqwest({
        url: this.baseUrl('cfg') + '/rest/queueList',
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: _.extend({ proxyToken: this.proxyToken(), ver: 'latest', format: 'json' }, params)
      });
    },

  }, abstract);

});