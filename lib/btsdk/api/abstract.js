define([
  'require',
  'btsdk/core',
  'btsdk/config'
], function (require, core, config) {
  'use strict';

  return {

    /**
     * Returns the logged-in user's proxyToken.
     *
     * Lazily requires the 'btsdk/user' module, due to circular dependencies: btsdk/user <-> btsdk/api/cfg.
     *
     * @return {string}
     */
    proxyToken: function () {
      return require('btsdk/user').token.proxyToken;
    },

    authUser: function () {
      return require('btsdk/user').authUser;
    },

    /**
     * Returns an object for Basic Auth header.
     *
     * @param  {string} username
     * @param  {string} password
     * @return {object}
     */
    basicAuthHeaders: function (username, password) {
      return {
        'Authorization': 'Basic ' + btoa(username + ':' + password)
      };
    },

    /**
     * Returns the proper base url for various APIs.
     *
     * @param  {string} api
     * @return {string}
     */
    baseUrl: function (api) {
      switch (api) {
        case 'cfg':
          return 'https://api-cfg.' + config.env + '/index.php';

        case 'edge':
          return 'https://api-edge.' + config.env + '/edge';

        case 'int':
          return 'wss://api-int.' + config.env + '/ws';

        case 'rpt':
          return 'https://api-rpt.' + config.env + '/index.php';

        default:
          console.error('[api] - unknown api', api);
          throw '[api] - unknown api ' + api;
      }
    },

  };
});