define([
  'require',
  'btsdk/core',
  'btsdk/api/abstract',
  'reqwest',
  'underscore',
], function (require, core, abstract, reqwest, _) {
  'use strict';

  return _.extend({

    /**
     * GET /rest/segment/{segmentID}?method=recording
     *
     * Returns a call recording for the given segmentID.
     * The segmentID parameter may be a `callRequestID` or `callID`.
     *
     * @param  {string} segmentID
     * @return {promise}
     */
    callRecording: function (segmentID) {
      return reqwest({
        url: this.baseUrl('rpt') + '/rest/segment/' + segmentID,
        crossOrigin: true,
        method: 'get',
        type: 'json',
        data: {
          ver: '2014-06-04',
          format: 'json',
          proxyToken: this.proxyToken(),
          method: 'recording',
          recordingType: 'call',
        }
      });
    },

  }, abstract);

});