define(function(require) {
  'use strict';

  var btsdk = require('btsdk/core');

  btsdk.sos  = require('btsdk/sos');
  btsdk.api  = require('btsdk/api');
  btsdk.sip  = require('btsdk/sip');
  btsdk.user = require('btsdk/user');

  return btsdk;
});
