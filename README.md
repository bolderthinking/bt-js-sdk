# Humach JavaScript SDK
---

This Humach JavaScript SDK gives developers a tool that makes integrating with the Humach platform simple and streamlined. The SDK is grouped into modules based on functionality. A Humach account with at least one endpoint address is required.

## Get the code

#### Download ZIP
You can download the SDK bundle in a ZIP archive here:

[https://bitbucket.org/bolderthinking/bt-js-sdk/downloads/btsdk-2.18.0.zip](https://bitbucket.org/bolderthinking/bt-js-sdk/downloads/btsdk-2.18.0.zip)

#### Using a CDN
If you do not want to download and host the SDK yourself, you may use our CDN at `code.humach.com`.

Minified: `<script src="//code.humach.com/code/btsdk-2.18.0.min.js"></script>`

Un-minified: `<script src="//code.humach.com/code/btsdk-2.18.0.js"></script>`

## Loading and starting

The SDK supports [RequireJS](http://requirejs.org) or other AMD-compatible loaders, and well as [VanillaJS](http://vanilla-js.com/). With either method, the variable `btsdk` is exported. All modules are available as properties of this variable, eg: `btsdk.sos` and `btsdk.sip`.

#### Vanilla JS
```
window.btAsyncInit = function () {
  // btsdk loaded and ready
};

(function () {
  var script = document.createElement("script");
  script.src = "//code.humach.com/code/btsdk-2.18.0.min.js";
  script.async = true;
  var e = document.getElementsByTagName("script")[0];
  e.parentNode.insertBefore(script, e);
})();
```

#### RequireJS / AMD
```
requirejs.config({
  ...
  paths: {
    ...
    "btsdk": "//code.humach.com/code/btsdk-2.18.0.min.js"
  }
});

require(["btsdk"], function (btsdk) {
  // btsdk loaded and ready
});
```

#Modules
---
### [btsdk](docs/btsdk.md)
This is the core module. It holds global SDK configuration and facilitates inter-module communication.

### [btsdk.sos](docs/btsdk-sos.md)
This module enables users to launch a Humach SOS widget on their website.

### [btsdk.sip](docs/btsdk-sip.md)
This module enables users to control a Humach SIP endpoint address.

### [btsdk.user](docs/btsdk-user.md)
This module handles user authentication. It provides many convenience methods for common user and endpoint actions.

### [btsdk.api.cfg](docs/btsdk-api-cfg.md)
API-CFG provides user and endpoint configuration data.

### [btsdk.api.rpt](docs/btsdk-api-rpt.md)
API-RPT provides reporting data.

### [btsdk.api.edge](docs/btsdk-api-edge.md)
API-EDGE provies two-way call data attachment.

### [btsdk.api.int](docs/btsdk-api-int.md)
API-INT provides real-time user and endpoint communication and control over WebSockets.

---
2016 © Humach - http://www.humach.com