<?php

// endpoint address credentials
$epa      = 'ENDPOINT_ADDRESS@edge.humach.com';
$password = 'ENDPOINT_ADDRESS_PASSWORD';

// request a single-use token
$url      = 'https://api-edge.humach.com/edge/rest/endpointAddress/' . urlencode($epa);
$headers  = array('Content-Type: application/json');
$httpBody = json_encode(array(
              'method' => 'generateToken',
              'ver'    => 'latest',
              'format' => 'json'
            ));

$ch = curl_init();

// endpoint address & password are used as basic-auth creds
curl_setopt($ch, CURLOPT_USERPWD, "{$epa}:{$password}");
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_POSTFIELDS, $httpBody);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$data = curl_exec($ch);
curl_close($ch);

$response = json_decode($data);

$token = isset($response->result->generateToken->token)
    ? $response->result->generateToken->token : null;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>JS SDK - SOS Example</title>
</head>
<body>
  <div id="btsdk-sos"></div>
  <div class="container">
    <h1>SOS Example</h1>
    <p>
      This example page shows how to configure and initialize the SOS module.
    </p>
    <p>
      It fetches a single-use SOS token (via cURL), and uses it to initialize the BTSDK, then the SOS module.
    </p>
  </div>

  <script>
  var btSosToken = <?= is_null($token) ? 'null' : "'{$token}'"; ?>;

  window.btAsyncInit = function () {
    if(btsdk.sos.acceptableBrowser() == false || !btSosToken) return;

    btsdk.init();

    btsdk.sos.init({
      address: "ENDPOINT_ADDRESS",
      // un-comment this to enable video calls between WebRTC endpoints
      // video: true,
      sos: {
        selectorId: "btsdk-sos",
        number: "NUMBER_TO_DIAL",
        token: btSosToken
      }
    });
  };

  // async load the sdk
  (function () {
    var script = document.createElement("script");
    script.src = "//code.humach.com/code/btsdk-2.18.0.min.js";
    script.async = true;
    var e = document.getElementsByTagName("script")[0];
    e.parentNode.insertBefore(script, e);
  })();
  </script>
</body>
</html>