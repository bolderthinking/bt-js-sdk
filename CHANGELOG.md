# CHANGELOG
---

## Version 2.18.0 (2017-04-14)

* Upgraded to JsSIP 3.0.7 (BTJS-104). This is an under-the-hood change and all existing public APIs remain the same.

## Version 2.17.0 (2017-03-30)

* The API-INT heartbeat is now enabled by default with a value of 30 seconds (BTJS-100).
* New `btsdk.sip` module event `btsdk.sip.icecandidate` fired for all ICE candidates (BTJS-101).
* New `btsdk.sip.init()` option `iceServers` to control what ICE servers are used (BTJS-102).
* See the [btsdk.sip](docs/btsdk-sip.md) documentation for more information.
* Updated documentation for `btsdk.api.int.error`. See the [btsd.api.int](docs/btsdk-api-int.md) documentation for more information.

## Version 2.16.0 (2017-02-02)

* **IMPORTANT** Fixes an issue introduced in Chrome 57+ that will completely break WebRTC calling (BTJS-99). **All previous SDK versions' WebRTC functionality will not work starting with Chrome 57 going forward.** We strongly urge all clients to upgrade as soon as possible.
* The data included on the `btsdk.sip.hangup` changed in this version (BTJS-94). Previously, it was simply a call ID string. This has been changed to an object like: `{callID: "123-call-eee0ff46-e8a5-11e6-88bc-0a745429f292-v3", cause: "Canceled"}`. For a list of possible hangup causes, see the [btsdk.sip](docs/btsdk-sip.md) doc page.
* Retire some unnecessary vendor libs (BTJS-98).

## Version 2.15.0 (2016-12-14)

* Upgraded to JsSIP 3.0 (BTJS-96). This is an under-the-hood change and all existing public APIs remain the same.
* **NOTE** The built `dist/` files in this version do not include 2.13.3 or 2.13.4 fixes. Please use version 2.16.0 which includes both.

## Version 2.14.0

Intentionally skipped.

## Version 2.13.4 (2016-12-14)

* Fixed a bug with certain vendor libs overwriting existing global versions (BTJS-97).

## Version 2.13.3 (2016-11-22)

* Fixed a bug where microphone stats collection would not start for outbound calls (BTJS-95).

## Version 2.13.2 (2016-10-20)

* Fixed a bug where `btsdk.user.authenticate` would fire on token renewal. This event will only be triggered after a call to `btsdk.user.authenticate()` or after loading a token supplied to `btsdk.init()`.

## Version 2.13.1 (2016-10-19)

* New API-EDGE commands, which allow supplying a `proxyToken` option for authentication. (BTJS-88)
    * `btsdk.sip.getCustomerCallData(cid, opts)`
    * `btsdk.sip.putCustomerCallData(cid, opts)`
    * `btsdk.sip.getSystemPublicCallData(cid, opts)`
* Better validaiton of `user` initialization option passed to `btsdk.init(opts)`.
    * `btsdk.user.authenticate` will be fired any time a valid token is created or loaded.
    * `btsdk.user.token_invalid` will be fired if the supplied initialization token is invalid.
    * `btsdk.user.token_create` will be fired any time a proxy token is created. The event data may be stored and passed to `btsdk.init()` as the `user` option.
* Updated `btsdk.api.int.unpauseRecording()` to `btsdk.api.int.resumeRecording()` and the associated event to `btsdk.call.resume_recording`.
    * The only valid argument to `btsdk.api.int.pauseRecording()` and `btsdk.api.int.resumeRecording()` changed from `callID` to `lobbyID`.

## Version 2.13.0 (2016-10-18)

* New API-EDGE command `btsdk.sip.getSystemPublicData()` to fetch system-public call-attached-data (BTJS-82).
    * See [docs/btsdk-sip](docs/btsdk-sip.md) for more information.
* New API-RPT command `btsdk.api.rpt.callRecording(segmentID)` to fetch call recordings for a given `segmentID`. (BTJS-83)
    * See [docs/btsdk-api-rpt](docs/btsdk-api-rpt.md) for more information.
* New API-CFG command `btsdk.user.loadQueueList()` to fetch the auth user's relevant queues. Triggers a `btsdk.user.queues_loaded` event once all queues have been fetched. (BTJS-85)
    * See [docs/btsdk-user](docs/btsdk-user.md) for more information.
* New API-INT commands `btsdk.api.int.pauseRecording(opts)` and `btsdk.api.int.unpauseRecording(opts)` for pausing/un-pausing screen recording. (BTJS-80)
    * See [docs/btsdk-api-int](docs/btsdk-api-int.md) for more information.
* The API-INT command `btsdk.api.int.dial(opts)` now accepts `queueID` as a valid parameter for dial-on-behalf-of-queue functionality. (BTJS-85)
    * See [docs/btsdk-api-int](docs/btsdk-api-int.md) for more information.
* New initialization option `micStats` for `btsdk.sip.init(opts)` which will enable microphone-volume statistics gathering capabilities. Defaults to `false`. (BTJS-84)
    * See [docs/btsdk-sip](docs/btsdk-sip.md) for more information.
* Default logging namespace will include lower-level libraries. (BTJS-79)

---

## Version 2.12.2 (2016-10-10)

* Increased WebSocket connection timeout value from 2s to 30s (BTJS-76).
* Better call-end session cleanup (BTJS-75).
* Fixed an issue where the `btsdk.sip.call_connected` event would not fire if the local audio/video stream element was missing (BTJS-74).

---

## Version 2.12.1 (2016-09-30)

* Fixed an issue with WebSocket reconnects using an outdated proxy token (BTJS-72).
    * Proxy token updates will also now be sent any time the WebSocket opens to ensure the most up-to-date token is used.
* All API-INT WebSocket messages will include a `ref` property to uniquely identify messages (BTJS-71).

---

## Version 2.12.0 (2016-09-12)

* Upgraded version of JsSIP (BTJS-69).
    * This has been internally tested and requires no implementation changes.
* Updated query params sent in `btsdk.api.cfg.interfaceProfileList` in preparation for upcoming changes (BTJS-70).

---

## Version 2.11.0 (2016-08-23)

* Default environment domain is now `humach.com` (BTJS-68).
* Added three new API-INT methods for MCB (Monitor, Coach, Barge) (BTJS-67).
    * `btsdk.api.int.mcbLogin(params)`
    * `btsdk.api.int.mcbAction(params)`
    * `btsdk.api.int.mcbLogout(params)`
    * See [docs/btsdk-api-int](docs/btsdk-api-int.md) for more information.

---

## Version 2.10.0 (2016-04-19)

* SDK will fire the `btsdk.sip.disconnected` event when the SIP WebSocket disconnects (BTJS-62).
* JsSIP STUN server address updated. This will significantly improve inbound call connection time (BTJS-64).

---

## Version 2.9.0 (2016-04-13)

* API-INT WebSocket will send periodic pings to help keep WS alive in firewalled networks (BTJS-60).
* JsSIP registration expiry time reduced to 60 seconds (BTJS-61).

---

## Version 2.8.0 (2016-04-13)

* API-INT WebSocket will attempt to auto-reconnect if the connection is closed (BTJS-59).
* SIP engine `sipml5` has been removed from the build. `jssip` is the default and only fully-supported engine.

---

## Version 2.7.2 (2016-03-25)

* Fixes a bug (BTJS-58) where the `btsdk.sip.ringback` event could be fired more than once for the same outbound call.

---

## Version 2.7.1 (2016-03-16)

* Fixes a bug (BTJS-57) where the `btsdk.sip.hangup` event would not include the callID.

---

## Version 2.7.0 (2016-03-01)

* The following SIP call events will now send data as well. Currently just the `callID` string.
    * `btsdk.sip.ringing`
    * `btsdk.sip.ringback`
    * `btsdk.sip.hangup`
    * `btsdk.sip.call_connected`

---

## Version 2.6.0 (2016-02-25)

* New event `btsdk.sip.ringback` which is fired when WebRTC phones should play ringback.
    * [docs/btsdk-sip](docs/btsdk-sip.md)
* New function `btsdk.destroy()` which will destroy all initialized modules and remove event listeners.
    * [docs/btsdk](docs/btsdk.md)

---

## Version 2.5.1 (2016-02-17)

* Fixes a bug (BTJS-53) where `btsdk.sip.destroy()` would cause sip-related events to not fire if the module was re-initialized.

---

## Version 2.5.0 (2016-02-04)

* Added new API-CFG methods `btsdk.api.user.loadSpeedDial()`, `btsdk.api.user.loadInterfaceProfile()`.
    * [docs/btsdk-api-cfg](docs/btsdk-api-cfg.md)
    * [docs/btsdk-user](docs/btsdk-user.md)

---

## Version 2.4.0 (2015-12-21)

* Updated parameters for `btsdk.api.int.hangup()` (`lobbyID`).
* Added new API-INT methods `btsdk.api.int.mute()`, `btsdk.api.int.unmute()`, `btsdk.api.int.dtmf()`, `btsdk.api.int.modifyLobby()`, `btsdk.api.int.blindTransfer()`.
    * [docs/btsdk-api-int](docs/btsdk-api-int.md)

---

## Version 2.3.0 (2015-11-10)

* Updated parameters for `btsdk.api.int.hangup()` (`persistLobby`, `releaseLobbyControl`).
* Added new API-INT methods `btsdk.api.int.split()` and `btsdk.api.int.join()`.
    * [docs/btsdk-api-int](docs/btsdk-api-int.md)

---

## Version 2.2.0 (2015-10-19)

* Fixes a bug (BTJS-47) with JsSIP engine mic permission events.
* Added the ability to load SDK with an existing user token.
* Added `btsdk.user.token_renew` and `btsdk.user.token_renew_error` events.
    * [docs/btsdk-user](docs/btsdk-user.md)

---

## Version 2.1.1 (2015-06-08)

* Fixes a bug (BTJS-41) with SOS CSS loading.

---

## Version 2.1.0 (2015-05-16)

* Updated `btsdk.user.stateLogin()` to `btsdk.user.stateLogin(endpointAddressID)`.

---

## Version 2.0.0 (2015-05-08)

* Moved most `btsdk.init(opts)` options to `btsdk.sip.init(opts)` and `btsdk.sos.init(opts)`.
    * [docs/btsdk-sip](docs/btsdk-sip.md)
    * [docs/btsdk-sos](docs/btsdk-sos.md)
* Added `user` module which handles authentication, endpoint listing, and user-related convenience functions.
    * [docs/btsdk-user](docs/btsdk-user.md)
* Separated API modules into `btsdk.api.cfg`, `btsdk.api.edge`, and `btsdk.api.int`.
    * [docs/btsdk-api-cfg](docs/btsdk-api-cfg.md)
    * [docs/btsdk-api-int](docs/btsdk-api-int.md)
    * [docs/btsdk-api-edge](docs/btsdk-api-edge.md)
* Fixes a bug (BTJS-36) with non-video SOS widget remote audio.

---

## Version 1.2.2 (2015-03-12)

* Fixes a bug (BTJS-35) with properly handling a call canceled by a remote party before it's answered.

---

## Version 1.2.1 (2015-02-26)

* New `btsdk.init()` option `video` which, when set to true, will enable video features in `btsdk.sip` and `btsdk.sos` modules.
* New SOS skin for video calling.
* Setting or fetching call-attached-data will return `null` if no call is up.

---

## Version 1.2.0 (2015-02-20)

* Added ability to answer inbound calls with JsSIP engine.
* New event `btsdk.sip.ringing` for receiving inbound calls.
* New method `btsdk.sip.answer()` to answer inbound calls.

---

## Version 1.1.2 (2015-02-18)

* Added `getCallAttachedData()` and `setCallAttachedData(key, value)` convenience methods to `btsdk.sip` module.
* Updated docs with examples in both modules.

---

## Version 1.1.1 (2015-02-11)

* Fixes a bug (BTJS-30) with setting call-attached-data when `env` option wasn't set.

---

## Version 1.1.0 (2015-01-30)

* Added audio mute capabilities.
* Added `api` module and call-attached-data capabilities.
* Added new sample scripts in the `examples/` directory.

---

## Version 1.0.0 (2015-01-26)

* Initial release. Includes SOS and basic SIP functionality.
