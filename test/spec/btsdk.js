// PhantomJS doesn't spupport bind yet
Function.prototype.bind = Function.prototype.bind || function (thisp) {
  var fn = this;
  return function () {
    return fn.apply(thisp, arguments);
  };
};

define(['btsdk'], function (btsdk) {

  describe('The SDK', function () {

    it('version number should be 2.18.0', function () {
      expect(btsdk.VERSION).toBe('2.18.0');
    });

  });

});