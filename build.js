// see a complete list of options here:
// https://github.com/jrburke/r.js/blob/master/build/example.build.js
requirejs.config({
  // all modules loaded are relative to this path
  // e.g. require(["grid/core"]) would grab /lib/grid/core.js
  baseUrl: "./lib",

  // specify custom module name paths
  paths: {
    "bean":            "../vendor/bean",
    "dragdealer":      "../vendor/dragdealer",
    "hark":            "../vendor/hark",
    "jquery":          "../vendor/jquery-2.1.3.min",
    "jquery-ui":       "../vendor/jquery-ui.min",
    "jquery-slideout": "../vendor/jquery.tabslideout.1.3",
    "jssip":           "../vendor/jssip",
    "moment":          "../vendor/moment",
    "reconnecting-ws": "../vendor/reconnecting-websocket",
    "reqwest":         "../vendor/reqwest",
    'sipjs':           '../vendor/sipjs',
    "sipml5":          "../vendor/sipml5",
    "spec":            "../test/spec",
    "tock":            "../vendor/tock",
    "underscore":      "../vendor/underscore",
  },

  // target amd loader shim as the main module, path is relative to baseUrl.
  name: "../vendor/almond",

  optimize: "none",

  // files to include along with almond.  only lib/btsdk.js is defined, as
  // it pulls in the rest of the dependencies automatically.
  include: ["btsdk"],

  // code to wrap around the start / end of the resulting build file
  // the global variable used to expose the API is defined here
  wrap: {
    start: "(function(global, define) {\n"+
              // check for amd loader on global namespace
           "  var globDefine = global.define;\n"+
           "  var globMoment = global.moment;\n"+
           "  var globUnderscore = global._;\n",

    end:   "  var library = require('btsdk');\n"+
           "  if(typeof module !== 'undefined' && module.exports) {\n"+
                // export library for node
           "    module.exports = library;\n"+
           "  } else if(globDefine) {\n"+
                // define library for global amd loader that is already present
           "    (function (define) {\n"+
           "      define(function () { return library; });\n"+
           "    }(globDefine));\n"+
           "  } else {\n"+
                // define library on global namespace for inline script loading
           "    global['btsdk'] = library;\n"+
           "  }\n"+
           "  global.moment = globMoment;\n"+
           "  global._ = globUnderscore;\n"+
           "}(this));\n"
  },

  // don't include coffeescript compiler in optimized file
  stubModules: [],

  // build file destination, relative to the build file itself
  out: "./dist/btsdk.js"
})