# btsdk
---

This is the SDK's core component. It must be initialized before any modules are used. It sets global SDK parameters and establishes event listeners.

# Methods

 - `btsdk.init(opts)` - Initializes the SDK with the supplied options.
 - `btsdk.destroy()` - Destroys the SDK and all initialized modules.

## btsdk.init(opts)

`opts` _object_ - An object containing the SDK initialization options. See below.

`opts.user` *object* [**optional**] - An existing object of user/token data. This loads the SDK in an authenticated state.

This should be of the same structure that is emitted from the `btsdk.user.token_create` event.

```
{
    "proxyToken": "hyBKaXEIy3xA...",
    "renewTS": "2016-06-15T21:18:23Z",
    "expireTS": "2016-06-20T21:18:23Z",
    "user": {
        ...
    }
}
```

`opts.env` *string* [**optional**] - An alternative environment to connect and register with.

Default: `humach.com`.

`opts.cdn` *string* [**optional**] - An alternative URL to load SDK assets from.

Default: `code.humach.com`.

`opts.api.int.heartbeatSecs` *int* [**optional**] - Interval to send API-INT WSS heartbeat pings (0 = disabled).

Default: `30`.

## btsdk.destroy()

A convenience function which destroys all initialized modules. Any WebSocket or SIP connections will be closed. All event listeners will be removed. The init function may be called again with new parameters to start it up again.

# Examples

### Initializing the SDK

The call to `btsdk.init(opts)` is required before anything else is done with the SDK. Normally, it will be as simple as this:

```
btsdk.init();
```

To load a stored bundle of user/token data, pass