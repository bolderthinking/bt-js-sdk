# btsdk.user
---

This module represents a user. It provides convenience methods for logging in the user, getting their status, subscribing for their events, etc.

# Methods
 - `btsdk.user.authenticate(username, password)`
 - `btsdk.user.loadEndpoints()`
 - `btsdk.user.loadSpeedDial()`
 - `btsdk.user.loadInterfaceProfile()`
 - `btsdk.user.loadQueueList()`
 - `btsdk.user.subscribe()`
 - `btsdk.user.unsubscribe()`
 - `btsdk.user.status()`
 - `btsdk.user.stateLogin(endpointAddressID)`
 - `btsdk.user.stateLogout()`
 - `btsdk.user.mcbLogin(endpointAddressID)`
 - `btsdk.user.mcbLogout()`
 - `btsdk.user.availableStates()`
 - `btsdk.user.stateChange(stateID, subStateID)`

## btsdk.user.authenticate(username, password)

`username` _string_ - The user's username.

`password` _string_ - The user's password.

Log a user in. Successful login results in a proxy token.

Triggers events `btsdk.user.authenticate` and `btsdk.user.token_create`.

## btsdk.user.loadEndpoints()

Convenience function to load the logged-in user's endpoint list.

Calls `btsdk.api.cfg.endpointList(userID)`.

Returns a promise.

## btsdk.user.loadSpeedDial()

Convenience function to load the logged-in user's speed dial list.

Calls `btsdk.api.cfg.speedDialList()`.

Returns a promise.

## btsdk.user.loadInterfaceProfile()

Loads the logged-in user's interface profile.

Calls `btsdk.api.cfg.speedDialList()`.

Returns a promise.

## btsdk.user.loadQueueList()

Convenience function to load the logged-in user's relevant queue list.

Calls `btsdk.api.cfg.queueList()`.

Returns a promise from `Promise.all()`.

## btsdk.user.subscribe()

Convenience function to subscribe to the logged-in user's events.

Calls `btsdk.api.int.subscribe(opts)`.

Triggers event `btsdk.user.subscribe`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.unsubscribe()

Convenience function to unsubscribe from the logged-in user's events.

Calls `btsdk.api.int.unsubscribe(opts)`.

Triggers event `btsdk.user.unsubscribe`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.status()

Convenience function to load the logged-in user's status. Shows ongoing calls and user state.

Calls `btsdk.api.int.userStatus(userID)`.

Triggers event `btsdk.user.user_status`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.stateLogin(endpointAddressID)

`endpointAddressID` _string_ - The endpoint address to use for agent calls.

Convenience function to log the logged-in user into state management. Uses `endpointAddressID` for agent calls.

Calls `btsdk.api.int.stateLogin(opts)`.

Triggers event `btsdk.user.state_login`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.stateLogout()

Convenience function to log the logged-in user out from state management.

Calls `btsdk.api.int.stateLogout(userID)`.

Triggers event `btsdk.user.state_logout`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.mcbLogin(endpointAddressID)

`endpointAddressID` _string_ - The endpoint address to use for MCB calls.

Convenience function to log the logged-in user in for MCB operations using the `endpointAddressID` for the MCB call.

Calls `btsdk.api.int.mcbLogin(opts)`.

Triggers event `btsdk.user.mcb_login`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.mcbLogout()

Convenience function to log the logged-in user out from MCB and ends their MCB call.

Calls `btsdk.api.int.mcbLogout(opts)`.

Triggers event `btsdk.user.mcb_logout`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.availableStates()

Convenience function to request a list of logged-in user's available state and sub-state combinations.

Calls `btsdk.api.int.availableStates(userID)`.

Triggers event `btsdk.user.available_states`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

## btsdk.user.stateChange(stateID, subStateID)

`stateID` _string_ - The major state ID.

`subStateID` _string_ - The sub-state ID.

Convenience function to request the logged-in user's state be changed.

Calls `btsdk.api.int.stateChange(opts)`.

Triggers event `btsdk.user.state_change`.

See the [btsdk.api.int](btsdk-api-int.md) docs for more information.

# Events

Events typically have a piece of data associated with them. See below each event for an example structure of the event data.

### `btsdk.user.authenticate`

Fired once the user has successfully authenticated.

```
{
  "userID": "277",
  "customerID": "3",
  "locationID": null,
  "username": "jennyjohnson",
  "stateID": "1",
  "agent": "1",
  "email": "jennyjohnson@example.com",
  "timezone": "America/Chicago",
  "firstName": "Jenny",
  "lastName": "Johnson",
  "middleName": "",
  "createdTS": "2011-03-18 18:50:04",
  "modifiedTS": "2017-02-07 22:44:24",
  "PIN": null,
  "custDefField1": "",
  "custDefField2": "",
  "recNameAudioResourceID": null,
  "masterUserID": null,
  "customerName": "Example",
  "profileImage": "",
  "countryID": null,
  "siteID": "3",
  "defaultInboundProfileID": "229",
  "defaultOutboundProfileID": null,
  "directory": "0",
  "agentAutoAnsQueueCalls": "0",
  "agentRejectCallsIfOnQueueCall": "0",
  "dialOnBehalfOfQueue": "0",
  "stateName": "Active",
  "siteName": "Default",
  "shadowUser": "0",
  "blockMCB": "0",
  "shadowUserCt": "0",
  "countryCode": "",
  "userType": "user",
  "enableUserPortal": "1",
  "versionKey": "p5Xr3UqC4GgvZAtxmkMmNw=="
}
```

### `btsdk.user.token_create`

Fired once the user has successfully authenticated.

This event includes the token data that may be temporarily saved to re-initialize the SDK across page reloads.

```
btsdk.on('btsdk.user.token_create', function (data) {
  window.localStorage.setItem('tokenData', JSON.stringify(data));
});
```

```
{
  "proxyToken": "mWmFdR4uANAuFQtQ4G9ZHYLYf80c1ztD5K72IPtbTeAEgMNoPEAV1ouy9epct7e3XvNT3AMNXlBn6OUiVuFZDrFMSmNPfK/oyNltyJ1kG3F1hnEgFmFxNvU+ky+gzMvo8Lja/TqehhTNCZJlXw+YrgMi1v9WQeb76GWWcx7rzwRaQ/1l4Jdkeb4PeCGWkOX1Su0XBbkINNoaElXmEi2VdbgKWUit09tbkqLdUL21/ErccRATEHxJLSqUB0xjTBVS9Z6Tq60rFnLfKezcjjCImTi5XrkpdidWtKjK9e7QGlIr7jE41dAeCE+oKLNJbUT/yPpQvzJqd7yx2Owbrt2i0TXS0JNnXH0e7PQqAvMbz54=",
  "expireTS": "2017-03-11T00:53:38Z",
  "renewTS": "2017-03-06T01:53:38Z",
  "user": {
    "userID": "277",
    "customerID": "3",
    "locationID": null,
    "username": "jennyjohnson",
    "stateID": "1",
    "agent": "1",
    "email": "jennyjohnson@example.com",
    "timezone": "America/Chicago",
    "firstName": "Jenny",
    "lastName": "Johnson",
    "middleName": "",
    "createdTS": "2011-03-18 18:50:04",
    "modifiedTS": "2017-02-07 22:44:24",
    "PIN": null,
    "custDefField1": "",
    "custDefField2": "",
    "recNameAudioResourceID": null,
    "masterUserID": null,
    "customerName": "Example",
    "profileImage": "",
    "countryID": null,
    "siteID": "3",
    "defaultInboundProfileID": "229",
    "defaultOutboundProfileID": null,
    "directory": "0",
    "agentAutoAnsQueueCalls": "0",
    "agentRejectCallsIfOnQueueCall": "0",
    "dialOnBehalfOfQueue": "0",
    "stateName": "Active",
    "siteName": "Default",
    "shadowUser": "0",
    "blockMCB": "0",
    "shadowUserCt": "0",
    "countryCode": "",
    "userType": "user",
    "enableUserPortal": "1",
    "versionKey": "p5Xr3UqC4GgvZAtxmkMmNw=="
  }
}
```

### `btsdk.user.token_invalid`

Fired when the token provided upon initialization is deemed invalid. The user will have to re-authenticate.

```
(no data)
```

### `btsdk.user.token_renew`

Fired when the proxy token is renewed.

```
{
  "eventTS": "2017-03-06T01:15:47.815Z",
  "token": {
    "proxyToken": "pAiSGM9d/f+32Mrqo1iNp9vW+w8YobxdgUXdLSRhCEOUqF/Qmio95cSk5aEH5YV4BQR/DPmmuzOYfvQ5UJt5sAuoV7zrN2LdvQaI0X6SQoIZBWgVvpE86voFaOs8NZFDEhoDPv9jc5cvfY4XpJN2Qyc/VMIqxR18QC5CyA8VJD4ljvdDczhIeTaQXOS10TVbdQ+kwWouMbd/FfaVP0Gxnx83mh5Tc126wVrOSy2qm2sNJxIprZoQl6wxvvgTVym/OOjn/6SNZnspF02nAg0/TlbqcZFP+UJyS+0CaTaZ2xgt9jjpgS685PsbyK0YMf5JTulK5G4Ucc0mh+IQubEeAhn6jkuxoX/TMGWLZjjPHlI=",
    "renewTS": "2017-03-06T02:15:47Z",
    "expireTS": "2017-03-11T01:14:47Z"
  }
}
```

### `btsdk.user.token_renew_error`

Fired when the proxy token fails to renew. The user will have to re-authenticate.

```
{
  "error": "Failed to renew proxy token",
  "eventTS": "2017-03-06T01:21:13.754Z"
}
```

### `btsdk.user.endpoints_loaded`

Fired once the user's endpoints are loaded.

```
{
  "endpoints": [
    {
      "endpointID": "569",
      "stateID": "1",
      "userID": "277",
      "siteID": "3",
      "phoneTypeID": null,
      "createdTS": "2016-03-31 04:21:58",
      "modifiedTS": null,
      "type": "standard",
      "name": "wow",
      "autoprovision": "0",
      "siteName": "Default",
      "userFirstName": "John",
      "userLastName": "Johnson",
      "autoprovisionConfigID": null,
      "versionKey": "6FiJUtNrBifmE20/b3B3gA==",
      "endpointAddressList": {
        "endpointAddress": [
          {
            "endpointAddressID": "580",
            "endpointID": "569",
            "outboundProfileID": "136",
            "emergencyProfileID": null,
            "createdTS": "2016-03-31 04:21:58",
            "modifiedTS": "2016-10-15 23:09:18",
            "agentCallDialTimeout": "15",
            "address": "c3e569a580@edge.humach.com",
            "authType": "standard",
            "password": "XXXXXXXXXX",
            "sipMediaProfile": "sip-webrtc",
            "outboundProfileName": "John Johnson",
            "emergencyProfileName": null,
            "compiledOutboundProfile": {
              "externalCallerID": null,
              "internalCallerID": null,
              "callRecording": "1",
              "screenRecording": null,
              "allowPremiumRate": "1",
              "allowInternational": "1",
              "allowExternal": "1",
              "countryAbbreviation": null,
              "callRecordingRetentionDays": "30",
              "musicOnHold": "da8bc1a0-d9f1-4c9a-9f06-3b94e020bab4",
              "voiceMailbox": null,
              "userID": "277",
              "userFirstName": "John",
              "userLastName": "Johnson"
            },
            "versionKey": "8jgN0XVZ2++b2OPF2NWZyQ=="
          }
        ]
      }
    }
  ]
}
```

### `btsdk.user.speed_dial_loaded`

Fired once the user's speed dial list is loaded.

```
{
  "speedDial": {
    "speedDialID": "4",
    "createdTS": "2016-03-22 15:56:19",
    "modifiedTS": null,
    "name": "Agent List",
    "numbers": [
      {
        "type": "phoneNumber",
        "number": "+17015554321",
        "label": "Escalations"
      },
      {
        "type": "phoneNumber",
        "number": "+17015551234",
        "label": "Retention"
      }
    ],
    "versionKey": "FEGnrzTg26hjVNt2B7ceUQ=="
  }
}
```

### `btsdk.user.interface_profile_loaded`

Fired once the user's interface profile is loaded.

```
{
  "interfaceProfile": {
    "type": "cloudphone",
    "fields": {
      "allowExtensionDials": "1",
      "allowLobbyDials": "1",
      "allowPhoneNumberDials": "1",
      "allowSpeedDials": "1",
      "autoAnswer": "0",
      "queueCallBadgeKeys": [
        "queueName",
        "callType"
      ],
      "salesforce": {
        "screenPop": {
          "enabled": true,
          "searchFields": [
            {
              "type": "call",
              "field": "origNumber"
            }
          ],
          "e164": false,
          "maxWaitMs": 750
        },
        "callLog": {
          "enabled": true,
          "e164": false,
          "saveFields": {
            "call": {
              "duration": "CallDurationInSeconds"
            },
            "cfp": [],
            "salesforce": {
              "caseID": "WhatId"
            }
          }
        }
      }
    }
  }
}
```

### `btsdk.user.queues_loaded`

Fired once the user's queues list is loaded.

```
{
  "queues": [
    {
      "queueID": "278",
      "name": "Support Queue",
      "stateID": "1",
      "createdTS": "2010-09-19 21:39:04",
      "modifiedTS": "2016-11-30 18:33:55",
      "queueTypeID": "1",
      "queueSubTypeID": "0",
      "skillID": "223",
      "skillName": "Support Skill",
      "priorityLevel": "1",
      "callRecording": "1",
      "screenRecording": "0",
      "callWrapTimer": "0",
      "delayBeforeCall": null,
      "minDialsInQueue": null,
      "screenPopURL": null,
      "scheduleID": null,
      "blockMCB": "0",
      "dialerAccessURL": null,
      "outboundProfileID": null,
      "agentRequestExpires": "0",
      "queueTypeName": "Inbound",
      "queueSubTypeName": "None",
      "musicOnHold": null,
      "inQueueTreatment": {
        "actions": null,
        "timeoutAction": null,
        "timeoutNumber": null,
        "timeoutSecs": null
      },
      "versionKey": "WKgCCyCzlUokgia0BiHpqw=="
    }
  ]
}
```

### `btsdk.user.error`

Fired when an error occurs in the `user` module.

# Example Usage

### Logging into state-mangement and chaging state

```
// init the sdk and establish some listeners
btsdk.init();

// once the user is logged in connect to the websocket
btsdk.on('btsdk.user.authenticate', function (data) {
  btsdk.api.int.wsConnect();
});

// when the websocket opens, log in for state management
btsdk.on('btsdk.int.ws_open', function () {
  btsdk.user.stateLogin();
});

// once logged in for state-management, change state
btsdk.on('btsdk.user.state_login', function (data) {
  // go ready
  btsdk.user.stateChange(1, 0);
});

// log the user in
btsdk.user.authenticate('USERNAME', 'PASSWORD');
```

### Making calls

```
// init the sdk and establish some listeners
btsdk.init();

// once the user is logged in connect to the websocket
btsdk.on('btsdk.user.authenticate', function (data) {
  btsdk.api.int.wsConnect();
});

// when the websocket opens, subscribe to the logged-in user
btsdk.on('btsdk.int.ws_open', function () {
  btsdk.api.int.subscribe({ userID: btsdk.user.authUser.userID });
});

// once subscribed to the user, make a dial
btsdk.on('btsdk.user.subscribe', function (data) {
  btsdk.api.int.dial({
    endpointAddressID: 'ENDPOINT_ADDRESS_ID',
    number: 'PHONE_NUMBER',
    outboundProfileID: 'OUTBOUND_PROFILE_ID'
  });
});

// listen for call events
btsdk.on('btsdk.call.call_event', function (data) {
  console.log('call event, type: ' + data.type)

  if(data.type == 'callStart') {
    console.log('call start, callID: ' + data.callID);
  }

  if(data.type == 'callAnswered') {
    console.log('call answered');
  }

  if(data.type == 'callHangup') {
    console.log('call hung up');
  }
});

// log the user in
btsdk.user.authenticate('USERNAME', 'PASSWORD');
```