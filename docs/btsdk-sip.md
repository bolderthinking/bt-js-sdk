# btsdk.sip
---

This module enables users to control a SIP endpoint address.

# Methods
 - `btsdk.sip.init(opts)` - Start the SIP module.
 - `btsdk.sip.register()` - Registers the EPA.
 - `btsdk.sip.call(number)` - Attempts an outbound call to the given `number`.
 - `btsdk.sip.answer()` - Answers an incoming call.
 - `btsdk.sip.hangup()` - Hangs up the current call.
 - `btsdk.sip.destroy()` - Stops SIP engine.
 - `btsdk.sip.muteMic()` - Mutes the microphone.
 - `btsdk.sip.unmuteMic()` - Un-mutes the microphone.
 - `btsdk.sip.micStats()` - Returns volume statistics about the current local stream.
 - `btsdk.sip.getCallID()` - Returns the current call ID.
 - `btsdk.sip.getCallAttachedData()` - Returns all customer call-attached-data for the current call.
 - `btsdk.sip.setCallAttachedData(key, value)` - Sets a customer call-attached-data key-value pair.
 - `btsdk.sip.getSystemPublicData()` - Returns all system-public call-attached-data for the current call.

## btsdk.sip.init(opts)

Initializes the SIP engine.

`opts` _object_ - A JavaScript object containing the SDK initialization options. See below.

`opts.address` *string* [**required**] - The endpoint address.

Example: May use full address `c1e23a456@edge.humach.com` or just `c1e23a456`.

`opts.video` *boolean* [**optional**] - If set to `true`, will enable video calling.

Default: `false`

`opts.iceServers` *array* [**optional**] - An array of ICE servers.

Default: `['stun:stun.l.google.com:19302']`

`opts.micStats` *boolean* [**optional**] - Enables microphone volume statistics gathering.

**WARNING**: This is an experimental feature and not recommended for production usage.

Default: `false`

`opts.selectors` *object* [**optional**] - An object containing selector IDs for local and remote audio/video elements.

`opts.selectors.streamRemote` *string* [**optional**] - The remote `<audio>` or `<video>` element ID.

Default: `"btsdk-stream-remote"`

`opts.selectors.streamLocal`  *string* [**optional**] - The local `<audio>` or `<video>` element ID.

Default: `"btsdk-stream-local"`

`opts.listeners` *object* [**optional**] - An object containing event listeners.

## btsdk.sip.register()

Registers the endpoint address using the configured `address` and `password`.

SIP REGISTER.

## btsdk.sip.call(number)

`number` *string* - The extension or PSTN number to dial.

Calls the designated `number`.

SIP INVITE.

## btsdk.sip.answer()

Answers an incoming call. Incoming calls trigger a `btsdk.sip.ringing` event.

Will fire a `btsdk.sip.call_connected` event once connected.

SIP INVITE ACK.

## btsdk.sip.hangup()

Hangs up an ongoing call.

SIP BYE.

## btsdk.sip.destroy()

Stops the SIP engine and disconnects from the WebSocket.

## btsdk.sip.muteMic()

Mutes the local audio stream so the remote party cannot hear it.

## btsdk.sip.unmuteMic()

Un-mutes the local audio stream so the remote party can hear it.

## btsdk.sip.micStats()

Returns an object of volume statistics about the current local stream.

**Note:** Must pass the `micStats` option to enable stats collecton. If the feature is not enabled, stats will not update.

Return value is an object.

* `volume` - Current volume measured in decibels (-100 = silence, 0 = loudest).

```
{
  volume: -83.93705749511719
}
```

## btsdk.sip.getCallID()

If a call is in progress, it will return the SIP call ID. Otherwise, returns `null`.

## btsdk.sip.getCallAttachedData()

Proxy to `btsdk.api.getCallAttachedData()`.

If a call is in progress, it will fetch all call-attached-data that is attached to the current call. Returns a promise.

## btsdk.sip.setCallAttachedData(key, value)

`key` _string_ - A unique data key name.

`value` _mixed_ - The data content. Can be a string, number, or even JSON object.

If a call is in progress, it will set the given `key` to the `value`. Returns a promise.

**Note**: Both `btsdk.sip.getCallAttachedData()` and `btsdk.sip.setCallAttachedData(key, value)` are asynchronous and return a promise.

## btsdk.sip.getSystemPublicData()

Proxy to `btsdk.api.fetchAllSystemPublicData()`.

If a call is in progress, it will fetch all system-public call-attached-data. Returns a promise.

#### Example

```
// Setting call-attached-data
var xhr = btsdk.sip.setCallAttachedData("item_sku", "abc123");

xhr.done(function (data) {
  // call-attached-data successfully set
})
.fail(function ()  {
  console.error("Error setting call-attached-data");
});

.
.
.

// Fetching call-attached-data
var xhr = btsdk.sip.getCallAttachedData();

xhr.done(function (data) {
  var cad = data.result.data;

  for(i = 0; i < cad.length; i++) {
    console.log("key",cad[i].key,"value",cad[i].value);
  }
})
.fail(function ()  {
  console.error("Error fetching call-attached-data");
});
```

# Events

Events consist of a string identifier, like `btsdk.sip.initialized` and may also pass data to any handler functions. This data may be a simple string, or an object with multiple properties.

#### `btsdk.sip.initialized`
Fired when the SIP engine is initialized and ready.

Data: *none*

#### `btsdk.sip.disconnected`
Fired when the SIP WebSocket disconnects.

Data: *none*

#### `btsdk.sip.registered`
Fired when the endpoint address successfully registers.

Data: *none*

#### `btsdk.sip.registration_failure`
Fired when the endpoint address registration fails.

Data: *none*

#### `btsdk.sip.mic_permission_requested`
Fired when microphone permission is requested.

Data: *none*

#### `btsdk.sip.mic_permission_accepted`
Fired when microphone permission is granted.

Data: *none*

#### `btsdk.sip.mic_permission_refused`
Fired when microphone permission is refused.

Data: *none*

#### `btsdk.sip.icecandidate`
Fired when an ICE candidate event is emitted on the RTCPeerConnection.

Data: Instance of `RTCIceCandidate` or `null`. A `null` candidate indicates that ICE gathering has finished.

See the WebRTC Specificiation documentation on the `RTCIceCandidate` Interface for details (https://www.w3.org/TR/webrtc/#rtcicecandidate-interface).

#### `btsdk.sip.ringing`
Fired when an incoming call arrives. Can be ignored/refused by calling `btsdk.sip.hangup()`.

Data: *string*
`callID` - The current call identifier causing ringing.

#### `btsdk.sip.ringback`
Fired when an outbound call should be receiving ringback. This is important for WebRTC software phones which need to play their own ringback. The ringback audio and implementation details are left to the consumer.

Data: *string*
`callID` - The current call identifier causing ringback.

#### `btsdk.sip.call_connected`
Fired when a call is successfully connected.

Data: *string*
`callID` - The current call identifier that has connected.

#### `btsdk.sip.hangup`
Fired when a call is ended. This event will also fire when a call fails, to ensure proper call-ended logic can execute.

Data: *object*
*string* `callID` - The current call identifier that has hung up.
*string* `cause` - The reason the call ended. See possible values below.

#### Hangup Causes

* `Terminated`
* `WebRTC Error`
* `Canceled`
* `No Answer`
* `Expires`
* `No ACK`
* `Dialog Error`
* `User Denied Media Access`
* `Bad Media Description`
* `RTP Timeout`

#### `btsdk.sip.destroy`
Fired when the SIP engine is stopped and destroyed.

Data: *none*

# Examples

The `address` and `password` options are required. You will likely also use listeners to react to events and manage endpoint address' state.

```
function event_init() {
  btsdk.sip.register();
};

...

function event_error() {
  console.log("an error occurred")
}

btsdk.init();

btsdk.sip.init({
  address: "ENDPOINT_ADDRESS@edge.humach.com",
  password "ENDPOINT_ADDRESS_PASSWORD",
  listeners: {
    "btsdk.sip.initialized": event_init,
    "btsdk.sip.registered": event_registered,
    "btsdk.sip.registration_failure": event_registration_failure,
    "btsdk.sip.call_connected": event_call_connected,
    "btsdk.sip.hangup": event_hangup,
    "btsdk.sip.destroy": event_destroy,
    "btsdk.sip.error": event_error
  }
});
```