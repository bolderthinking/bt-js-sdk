# btsdk.api.edge
---

This module represents the HTTP API EDGE.

# Methods
 - `btsdk.api.edge.fetchAllCallAttachedData(cid, epa, pass)`
 - `btsdk.api.edge.setCallAttachedData(cid, epa, pass, key, value)`
 - `btsdk.api.edge.fetchAllSystemPublicData(cid, epa, pass)`
 - `btsdk.api.edge.getCustomerCallData(cid, opts)`
 - `btsdk.api.edge.putCustomerCallData(cid, opts)`
 - `btsdk.api.edge.getSystemPublicCallData(cid, opts)`

### *Note!*

* The `fetchAllCallAttachedData`, `setCallAttachedData`, and `fetchAllSystemPublicData`  methods require an `epa` + `pass` combination for use-cases where the SIP module is used as a standalone component.

* The `getCustomerCallData`, `putCustomerCallData`, and `getSystemPublicCallData` methods allow passing a `proxyToken` parameter inside its options for authentication.

## btsdk.api.edge.fetchAllCallAttachedData(cid, epa, pass)

`cid` _string_ - Call ID.

`epa` _string_ - Endpoint address.

`pass` _string_ - Endpoint password.

Fetches all customer call-attached-data that for the given call. Returns a promise.

## btsdk.api.edge.setCallAttachedData(cid, epa, pass, key, value)

`cid` _string_ - Call ID.

`epa` _string_ - Endpoint address.

`pass` _string_ - Endpoint password.

`key` _string_ - Data key or name.

`value` _mixed_ - A number, string, object, or any JSON-encodable value.

Sets a customer call-attached-data key and value. The value will be JSON-encoded. Returns a promise.

Setting an existing `key` will overwrite the `value`.

## btsdk.api.edge.fetchAllSystemPublicData(cid, epa, pass)

`cid` _string_ - Call ID.

`epa` _string_ - Endpoint address.

`pass` _string_ - Endpoint password.

Fetches all system-public data that is attached to a given call. Returns a promise.

----

## btsdk.api.edge.getCustomerCallData(cid, opts)

`cid` _string_ - Call ID.

`opts.proxyToken` _string_ - User proxy token for authentication.

`opts.key` _string_ [**optional**] - The specific data-key to fetch. Defaults to `all`.

Fetches all customer call-attached-data that for the given call. Returns a promise, or null if no `cid` is provided.

## btsdk.api.edge.putCustomerCallData(cid, opts)

`cid` _string_ - Call ID.

`opts.proxyToken` _string_ - User proxy token for authentication.

`opts.key` _string_ - Data key or name.

`opts.value` _mixed_ - A number, string, object, or any JSON-encodable value.

Sets a customer call-attached-data key and value. The value will be JSON-encoded. Returns a promise, or null if no `cid` is provided.

Setting an existing `key` will overwrite the `value`.

## btsdk.api.edge.getSystemPublicCallData(cid, opts)

`cid` _string_ - Call ID.

`opts.proxyToken` _string_ - User proxy token for authentication.

`opts.key` _string_ [**optional**] - The specific data-key to fetch. Defaults to `all`.

Fetches all system-public data that is attached to a given call. Returns a promise, or null if no `cid` is provided.