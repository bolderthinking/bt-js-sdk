# btsdk.api.int
---

This module represents the WebSocket API INT. This API provides real-time messaging to observe and control users and calls.

# Methods
 - `btsdk.api.int.wsConnect()`
 - `btsdk.api.int.wsClose()`
 - `btsdk.api.int.subscribe(opts)`
 - `btsdk.api.int.unsubscribe(opts)`
 - `btsdk.api.int.userStatus(userID)`
 - `btsdk.api.int.stateLogin(opts)`
 - `btsdk.api.int.stateLogout(userID)`
 - `btsdk.api.int.mcbLogin(opts)`
 - `btsdk.api.int.mcbLogout(opts)`
 - `btsdk.api.int.mcbAction(opts)`
 - `btsdk.api.int.availableStates(userID)`
 - `btsdk.api.int.stateChange(opts)`
 - `btsdk.api.int.endpointAddressStatus(opts)`
 - `btsdk.api.int.dial(opts)`
 - `btsdk.api.int.hold(opts)`
 - `btsdk.api.int.unhold(opts)`
 - `btsdk.api.int.mute(opts)`
 - `btsdk.api.int.unmute(opts)`
 - `btsdk.api.int.pauseRecording(opts)`
 - `btsdk.api.int.resumeRecording(opts)`
 - `btsdk.api.int.dtmf(opts)`
 - `btsdk.api.int.modifyLobby(opts)`
 - `btsdk.api.int.hangup(opts)`
 - `btsdk.api.int.blindTransfer(opts)`
 - `btsdk.api.int.join(opts)`
 - `btsdk.api.int.split(opts)`

## btsdk.api.int.wsConnect()

Connects the WebSocket to prepare sending/receiving API-INT messages.

Triggers event `btsdk.int.ws_open`.

## btsdk.api.int.wsClose()

Closes the WebSocket connection.

Triggers event `btsdk.int.ws_close`.

## btsdk.api.int.subscribe(opts)

`opts.userID` _mixed_ - A string (or array of) userID(s) to subscribe to.

`opts.callID` _mixed_ - A string (or array of) callID(s) to subscribe to.

Subscribes to users or calls. You may subscribe to multiples of either by providing an array of strings rather than a single string.

Triggers event `btsdk.user.subscribe`.

## btsdk.api.int.unsubscribe(opts)

`opts.userID` _mixed_ - A string (or array of) userID(s) to unsubscribe from.

`opts.callID` _mixed_ - A string (or array of) callID(s) to unsubscribe from.

Unsubscribes to users or calls. You may unsubscribe to multiples of either by providing an array of strings rather than a single string.

Triggers event `btsdk.user.unsubscribe`.

## btsdk.api.int.userStatus(userID)

Requests a given `userID` status.

Triggers event `btsdk.user.user_status`.

## btsdk.api.int.stateLogin(opts)

`opts.userID` _string_ - The user to log in.

`opts.endpointAddressID` _string_ - The endpoint address to use for agent calls.

Logs the given `userID` in for state management using the `endpointAddressID` for calls.

Triggers event `btsdk.user.state_login`.

## btsdk.api.int.stateLogout(userID)

Logs the given `userID` out from state management.

Triggers event `btsdk.user.state_logout`.

## btsdk.api.int.mcbLogin(opts)

`opts.userID` _string_ - The user to log in.

`opts.endpointAddressID` _string_ - The endpoint address to use for MCB calls.

Logs the given `userID` in for MCB operations using the `endpointAddressID` for the MCB call.

Triggers event `btsdk.user.mcb_login`.

## btsdk.api.int.mcbLogout(opts)

`opts.userID` _string_ - The user to log out.

Logs the given `userID` out from MCB and ends their MCB call.

Triggers event `btsdk.user.mcb_logout`.

## btsdk.api.int.mcbAction(opts)

`opts.userID` _string_ - The MCB user taking an action.
`opts.targetUserID` _string_ - The agent the action targets.
`opts.action` _string_ - The MCB action: "monitor", "coach", "barge", or "end".

Performs `action` for `userID` against the `targerUserID`.

Triggers event `btsdk.user.mcb_action`.

## btsdk.api.int.availableStates(userID)

Requests a list of available state + sub-state combinations for the given `userID`.

Triggers event `btsdk.user.available_states`.

## btsdk.api.int.stateChange(opts)

`opts.userID` _string_ - The `userID` whose state to change.

`opts.stateID` _string_ - The major state ID.

`opts.subStateID` _string_ - The sub-state ID.

Triggers event `btsdk.user.state_change`.

## btsdk.api.int.endpointAddressStatus(endpointAddress)

Requests an `endpointAddress` status.

## btsdk.api.int.dial(opts)

`opts.endpointAddressID` _string_ - The `endpointAddressID` to use for the call.

`opts.number` _string_ - The phone number to dial.

`opts.outboundProfileID` _string_ - The `outboundProfileID` to use for the call.

`opts.lobbyID` _string_ - An existing `lobbyID` to attach this call to.

`opts.queueID` _string_ - An existing (outbound) `queueID` to dial on behalf of.

## btsdk.api.int.hold(callID)

`callID` _string_ - The `callID` to place on hold.

Triggers event `btsdk.call.hold`.

## btsdk.api.int.unhold(callID)

`callID` _string_ - The `callID` to take off of hold.

Triggers event `btsdk.call.unhold`.

## btsdk.api.int.mute(callID)

`callID` _string_ - The `callID` to mute.

Triggers event `btsdk.call.mute`.

## btsdk.api.int.unmute(callID)

`callID` _string_ - The `callID` to unmute.

Triggers event `btsdk.call.unmute`.

## btsdk.api.int.pauseRecording(opts)

`opts.lobbyID` _string_ - The `lobbyID` for which to pause recording.

Triggers event `btsdk.call.pause_recording`.

## btsdk.api.int.resumeRecording(opts)

`opts.lobbyID` _string_ - The `lobbyID` for which to resume recording.

Triggers event `btsdk.call.resume_recording`.

## btsdk.api.int.dtmf(opts)

`opts.callID` _string_ - The `callID` to send DTMF on.

`opts.digit` _string_ - The digit press to send.  One of 0-9, *, #.

Triggers event `btsdk.call.dtmf`.

## btsdk.api.int.modifyLobby(opts)

`opts.lobbyID` _string_ - The `lobbyID` of the lobby to modify.

`opts.persistLobby` _bool_ - Keeps the call's lobby from being destroyed.

`opts.releaseLobbyControl` _bool_ - Releases the call's lobby from the current user's control.

Triggers event `btsdk.lobby.modify_lobby`.

## btsdk.api.int.hangup(opts)

`opts.callID` _string_ - The `callID` to hang up.

`opts.lobbyID` _string_ - An optional param to hang up all calls in `lobbyID`.

`opts.persistLobby` _bool_ - Keeps the call's lobby from being destroyed.

Default: `false`

`opts.releaseLobbyControl` _bool_ - Releases the call's lobby from the current user's control.

Default: `false`

Triggers event `btsdk.call.hangup`.

## btsdk.api.int.blindTransfer(opts)

`opts.number` _string_ - The phone number to dial.

`opts.callID` _string_ | _array_ - The `callID` of one or more calls to transfer.

Triggers event `btsdk.call.blind_transfer`.

## btsdk.api.int.join(opts)

`opts.callID` _string_ | _array_ - The `callID` of one or more calls to join.

`opts.lobbyID` _string_ - The `lobbyID` to join the call(s) to.

`opts.releaseLobbyControl` _bool_ - Releases the call's lobby from the current user's control.

Triggers event `btsdk.call.join`.

## btsdk.api.int.split(opts)

`opts.callID` _string_ | _array_ - The `callID` of one or more calls to "split" into a new lobby.

`opts.releaseLobbyControl` _bool_ - Releases the call's lobby from the current user's control.

Default: `false`

Triggers event `btsdk.call.split`.

# Events

Events typically have a piece of data associated with them. See below each event for an example structure of the event data.

### `btsdk.int.ws_open`

Fired when the WebSocket opens.

```
(no data)
```

### `btsdk.int.ws_close`

Fired when the WebSocket closes.

```
(no data)
```

### `btsdk.int.ws_message_receive`

Fired on all received WebSocket messages. Useful for debugging.

### `btsdk.int.ws_message_send`

Fired on all sent WebSocket messages. Useful for debugging.

### `btsdk.user.subscribe`

Fired when a user subscribe event is accepted.

```
{
  "userID": "277"
}
```

### `btsdk.user.unsubscribe`

Fired when a user unsubscribe event is accepted.

```
{
  "userID": "277"
}
```

### `btsdk.user.user_status`

Fired when a user status event is accepted.

Example user status for a user that is not logged into state management, and has no calls.

```
{
  "userID": "277",
  "state": null,
  "pendingState": null,
  "mcbSession": null,
  "calls": {},
  "lobbies": {}
}
```

Example user status for a user that is logged into state management, and has an active call.

```
{
  "userID": "277",
  "state": {
    "stateID": "6",
    "subStateID": "0",
    "stateName": "Busy",
    "subStateName": "none",
    "endpointAddressID": "580"
  },
  "pendingState": null,
  "mcbSession": null,
  "calls": {
    "3-call-0ae4a4e6-004c-11e7-9037-0a7181c34328-v3": {
      "instigatorID": "3-call-0a68f74c-004c-11e7-8790-1e8b0000076b-v3",
      "callID": "3-call-0ae4a4e6-004c-11e7-9037-0a7181c34328-v3",
      "callStatus": "answered",
      "lobbyID": "0aa85ab8-004c-11e7-9037-0a7181c34328",
      "lobbyPersist": false,
      "callStart": "2017-03-03T20:00:26Z",
      "callAnswer": "2017-03-03T20:00:27Z",
      "origNumber": "+17015554321",
      "destNumber": "c3e569a580@edge.humach.com",
      "callerID": "+17015554321",
      "callClassType": "agent",
      "mute": false,
      "hold": false,
      "endpointID": "569",
      "endpointAddressID": "580",
      "endpointAddress": "c3e569a580@edge.humach.com",
      "userID": "277",
      "agentQueueID": "278",
      "agentCallRequestID": "3-queue-0a08b774-004c-11e7-9037-0a7181c34328-v3",
      "agentCallWrapTimer": "0",
      "agentCallWrapState": "1",
      "agentCallWrapSubState": "0",
      "mcbSession": false,
      "callRecordingStatus": "recording",
      "screenRecordingStatus": "disabled",
      "customerID": "3"
    },
    "3-call-0a68f74c-004c-11e7-8790-1e8b0000076b-v3": {
      "instigatorID": "3-call-0a68f74c-004c-11e7-8790-1e8b0000076b-v3",
      "callID": "3-call-0a68f74c-004c-11e7-8790-1e8b0000076b-v3",
      "callStatus": "connected",
      "lobbyID": "0aa85ab8-004c-11e7-9037-0a7181c34328",
      "lobbyPersist": false,
      "callStart": "2017-03-03T20:00:25Z",
      "callAnswer": "2017-03-03T20:00:27Z",
      "origNumber": "+17015554321",
      "destNumber": "c3e569a580@edge.humach.com",
      "callerID": "+17015554321",
      "callClassType": "queue",
      "mute": false,
      "hold": false,
      "userID": "277",
      "mcbSession": false,
      "callRecordingStatus": "disabled",
      "screenRecordingStatus": "disabled",
      "customerID": "3"
    }
  },
  "lobbies": {
    "0aa85ab8-004c-11e7-9037-0a7181c34328": {
      "persist": false,
      "calls": [
        "3-call-0ae4a4e6-004c-11e7-9037-0a7181c34328-v3",
        "3-call-0a68f74c-004c-11e7-8790-1e8b0000076b-v3"
      ]
    }
  }
}
```

### `btsdk.user.state_login`

Fired when a user logs in for state management.

```
{
  "userID": "277",
  "endpointAddressID": "580",
  "eventTS": "2017-03-03T20:09:12Z"
}
```

### `btsdk.user.state_logout`

Fired when a user logs out from state management.

```
{
  "userID": "277",
  "eventTS": "2017-03-03T20:09:34Z"
}
```

### `btsdk.user.mcb_login`

Fired when a user logs into MCB.

### `btsdk.user.mcb_logout`

Fired when a user logs out from MCB.

### `btsdk.user.mcb_action`

Fired when a user performs an MCB action.

### `btsdk.user.state_change`

Fired when a user's state changes.

Example state change to "Ready".

```
{
  "eventTS": "2017-03-03T20:10:03Z",
  "pending": "0",
  "locked": "0",
  "userID": "277",
  "stateID": "1",
  "subStateID": "0",
  "stateName": "Ready",
  "subStateName": "None"
}
```

Example state change to "Assigned".

```
{
  "eventTS": "2017-03-03T20:10:36Z",
  "pending": "0",
  "locked": "0",
  "userID": "277",
  "stateID": "4",
  "subStateID": "0",
  "stateName": "Assigned",
  "subStateName": "none"
}
```

Example state change to "Busy".

```
{
  "eventTS": "2017-03-03T20:10:40Z",
  "pending": "0",
  "locked": "1",
  "userID": "277",
  "stateID": "6",
  "subStateID": "0",
  "stateName": "Busy",
  "subStateName": "none"
}
```

### `btsdk.user.available_states`

Fired when available states are requested.

```
{
  "eventTS": "2017-03-03T20:19:13Z",
  "states": [
    {
      "stateID": "1",
      "userID": "277",
      "subStateID": "0",
      "stateName": "Ready",
      "subStateName": "None"
    },
    {
      "stateID": "2",
      "userID": "277",
      "subStateID": "3",
      "stateName": "Not Ready",
      "subStateName": "Break"
    },
    {
      "stateID": "2",
      "userID": "277",
      "subStateID": "4",
      "stateName": "Not Ready",
      "subStateName": "Call Wrap"
    }
  ]
}
```

### `btsdk.call.subscribe`

Fired when a call subscribe event is accepted.

```
{
  "callID": "3-call-d5f0cfb4-004e-11e7-8616-1e8b00000dfe-v3"
}
```

### `btsdk.call.unsubscribe`

Fired when a call unsubscribe event is accepted.

```
{
  "callID": "3-call-d5f0cfb4-004e-11e7-8616-1e8b00000dfe-v3"
}
```

### `btsdk.call.dial`

Fired when a dial event is accepted.

```
{
  "callID": "3-call-61ed4c7c-0202-11e7-bd8c-0e96048894c2-v3",
  "eventTS": "2017-03-06T00:18:11Z",
  "endpointAddress": "c3e336a347@edge.humach.com",
  "endpointType": "standard",
  "endpointID": "336",
  "number": "+17015554321",
  "endpointAddressID": "347"
}
```

### `btsdk.call.hold`

Fired when a hold event is accepted.

```
{
  "callID": "3-call-693a84c4-0057-11e7-9037-0a7181c34328-v3",
  "eventTS": "2017-03-03T21:22:14Z"
}
```

### `btsdk.call.unhold`

Fired when an unhold event is accepted.

```
{
  "callID": "3-call-693a84c4-0057-11e7-9037-0a7181c34328-v3",
  "eventTS": "2017-03-03T21:22:29Z"
}
```

### `btsdk.call.mute`

Fired when a mute event is accepted.

```
{
  "callID": "3-call-693a84c4-0057-11e7-9037-0a7181c34328-v3",
  "eventTS": "2017-03-03T21:22:43Z"
}
```

### `btsdk.call.unmute`

Fired when an unmute event is accepted.

```
{
  "callID": "3-call-693a84c4-0057-11e7-9037-0a7181c34328-v3",
  "eventTS": "2017-03-03T21:22:50Z"
}
```

### `btsdk.call.pause_recording`

Fired when a pause recording event is accepted.

```
{
  "lobbyID": "68fde6f4-0057-11e7-9037-0a7181c34328",
  "eventTS": "2017-03-03T21:26:05Z"
}
```

### `btsdk.call.resume_recording`

Fired when an un-pause recording event is accepted.

```
{
  "lobbyID": "68fde6f4-0057-11e7-9037-0a7181c34328",
  "eventTS": "2017-03-03T21:26:19Z"
}
```

### `btsdk.call.hangup`

Fired when a hangup event is accepted.

Example when a `lobbyID` is used in the hangup command.

```
{
  "callID": "",
  "lobbyID": "68fde6f4-0057-11e7-9037-0a7181c34328",
  "releaseLobbyControl": false,
  "persistLobby": false,
  "eventTS": "2017-03-03T21:26:35Z"
}
```

### `btsdk.call.blind_transfer`

Fired when a blind transfer event is accepted.

### `btsdk.call.join`

Fired when one or more calls join a lobby.

```
{
  "lobbyID": "aa05a936-005a-11e7-9037-0a7181c34328",
  "callID": "3-call-9c3409a6-005a-11e7-955e-1e8b0000367e-v3",
  "eventTS": "2017-03-03T21:46:39Z"
}
```

### `btsdk.call.split`

Fired when one or more calls is split to a new lobby.

```
{
  "callID": "3-call-9c3409a6-005a-11e7-955e-1e8b0000367e-v3",
  "releaseLobbyControl": false,
  "eventTS": "2017-03-03T21:45:05Z"
}
```

### `btsdk.call.dtmf`

Fired when a call sends a DTMF signal.

```
{
  "callID": "3-call-305a4616-0058-11e7-aedb-1e8b00002c56-v3",
  "digit": "5",
  "eventTS": "2017-03-03T21:28:27Z"
}
```

### `btsdk.call.call_event`

Fired when a call event is received.

There are several types of call events. Each event will have a `type` property with one of these values:

* `callStart`
* `callAnswered`
* `callConnection`
* `callStatusUpdate`
* `callHangup`

#### Call Start (caller leg)

```
{
  "type": "callStart",
  "endpointID": "",
  "callerID": "+17015554321",
  "callRecordingStatus": "disabled",
  "callClassType": "queue",
  "customerID": "3",
  "lobbyID": "27fac13e-005e-11e7-9037-0a7181c34328",
  "callHandlerAddress": "172.18.6.95",
  "mute": false,
  "lobbyPersist": false,
  "eventTS": "2017-03-03T22:10:05Z",
  "hold": false,
  "endpointAddress": "",
  "endpointAddressID": "",
  "instigatorID": "3-call-27da6f42-005e-11e7-8645-1e8b00004589-v3",
  "origNumber": "+17015554321",
  "callID": "3-call-27da6f42-005e-11e7-8645-1e8b00004589-v3",
  "screenRecordingStatus": "disabled",
  "userID": "277",
  "destNumber": "c3e569a580@edge.humach.com",
  "callStatus": "initiating",
  "mcbSession": false
}
```

#### Call Start (endpoint leg)

```
{
  "type": "callStart",
  "endpointAddress": "c3e569a580@edge.humach.com",
  "hold": false,
  "eventTS": "2017-03-03T22:10:06Z",
  "mute": false,
  "lobbyPersist": false,
  "callHandlerAddress": "172.18.6.95",
  "customerID": "3",
  "lobbyID": "27fac13e-005e-11e7-9037-0a7181c34328",
  "agentCallWrapSubState": "0",
  "agentCallRequestID": "3-queue-2236e17e-005e-11e7-9037-0a7181c34328-v3",
  "callClassType": "agent",
  "callRecordingStatus": "recording",
  "agentCallWrapState": "1",
  "endpointID": "569",
  "callerID": "+17015554321",
  "mcbSession": false,
  "callStatus": "initiating",
  "screenRecordingStatus": "disabled",
  "userID": "277",
  "destNumber": "c3e569a580@edge.humach.com",
  "agentQueueID": "278",
  "instigatorID": "3-call-27da6f42-005e-11e7-8645-1e8b00004589-v3",
  "agentCallWrapTimer": "0",
  "callID": "3-call-28370658-005e-11e7-9037-0a7181c34328-v3",
  "origNumber": "+17015554321",
  "endpointAddressID": "580"
}
```

#### Call Answered

```
{
  "type": "callAnswered",
  "callStatus": "answered",
  "lobbyPersist": false,
  "eventTS": "2017-03-03T22:10:10Z",
  "callID": "3-call-28370658-005e-11e7-9037-0a7181c34328-v3",
  "releaseCall": false,
  "userID": "277",
  "lobbyID": "27fac13e-005e-11e7-9037-0a7181c34328"
}
```

#### Call Connection

```
{
  "type": "callConnection",
  "eventTS": "2017-03-03T22:10:10Z",
  "lobbyID": "27fac13e-005e-11e7-9037-0a7181c34328",
  "userID": "277",
  "callStatus": "initiating",
  "callID": "3-call-27da6f42-005e-11e7-8645-1e8b00004589-v3",
  "lobbyPersist": false
}
```

#### Call Status Update

```
{
  "type": "callStatusUpdate",
  "callID": "3-call-27da6f42-005e-11e7-8645-1e8b00004589-v3",
  "callRecordingStatus": "disabled",
  "lobbyID": "27fac13e-005e-11e7-9037-0a7181c34328",
  "screenRecordingStatus": "disabled",
  "userID": "277",
  "releaseCall": false,
  "mute": true,
  "lobbyPersist": false,
  "callStatus": "answered",
  "hold": true,
  "eventTS": "2017-03-03T22:10:29Z"
}
```

#### Call Hangup

```
{
  "type": "callHangup",
  "hangupReason": "ansHangUpOrig",
  "callStatus": "",
  "hangupSource": "origin",
  "callID": "3-call-27da6f42-005e-11e7-8645-1e8b00004589-v3",
  "eventTS": "2017-03-03T22:10:35Z",
  "userID": "277"
}
```

### `btsdk.lobby.modify_lobby`

Fired when a lobby has been modified.

```
{
  "lobbyID": "90554182-0059-11e7-9037-0a7181c34328",
  "releaseLobbyControl": false,
  "persistLobby": true,
  "eventTS": "2017-03-03T21:37:45Z"
}
```

### `btsdk.api.int.error`

 Fired when an error occurs in the `api.int` module.

Example when attempting to hang up a non-existent call leg.

```
{
  "event": "hangup",
  "error": "Invalid event: '277' ",
  "eventTS": "2017-03-03T21:58:50Z",
  "userID": "277"
}
```

Starting with a platform release slated for late April 2017, error events will be slightly changing.

```
{
  "event": "stateLogin",
  "error": "ALREADY_LOGGED_IN",
  "errorCode": 400,
  "errorText": "Already logged in"
  "eventTS": "2017-03-28T16:19:07Z",
  "userID": "277"
}
```

The `error` property will now be a const-style error string. The human-readable error will move to the new `errorText` property. The `errorCode` will be used in future updates to help differentiate client errors (400-level) and server errors (500-level).