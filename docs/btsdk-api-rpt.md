# btsdk.api.rpt
---

This module represents the HTTP API RPT.

# Methods
 - `btsdk.api.rpt.callRecording(segmentID)`

## btsdk.api.rpt.callRecording(segmentID)

`segmentID` _string_ - The `segmentID` of the call recording. May be an `agentCallRequestID` or a `callID`.

Retrieves a call recording url for the given `segmentID`. Returns a promise.
