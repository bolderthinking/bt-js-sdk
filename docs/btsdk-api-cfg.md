# btsdk.api.cfg
---

This module represents the HTTP API CFG.

# Methods
 - `btsdk.api.cfg.createProxyToken(username, password)`
 - `btsdk.api.cfg.renewProxyToken()`
 - `btsdk.api.cfg.endpointList(userID)`
 - `btsdk.api.cfg.speedDialList()`
 - `btsdk.api.cfg.interfaceProfileList(userID)`
 - `btsdk.api.cfg.queueList(params)`

## btsdk.api.cfg.createProxyToken(username, password)

`username` _string_ - The user's username.

`password` _string_ - The user's password.

Generates a proxy token for a user. Returns a promise.

## btsdk.api.cfg.renewProxyToken()

Renews the current proxy token (before it expires). Returns a promise.

## btsdk.api.cfg.endpointList(userID)

Requests a list of endpoints that belong to `userID`. Returns a promise.

## btsdk.api.cfg.speedDialList()

Requests the speed dial list. Returns a promise.

## btsdk.api.cfg.interfaceProfileList(userID)

Requests the interface profile list. Returns a promise.

## btsdk.api.cfg.queueList(params)

Requests a queue list. See API-CFG documentation for acceptable `params`. Returns a promise.