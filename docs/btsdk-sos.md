# btsdk.sos
---

This module enables users to launch an SOS widget on their website. The SOS module requires use of an SOS-compatible endpoint address.

This functionality is designed to be used on public-facing websites. Exposing an endpoint address password poses a security risk. This is mitigated by the SOS module using single-use tokens to authenticate outbound calls. These tokens are invalidated once a call attempt is performed.

To protect the endpoint address password and generate a single-use tokens, a server-side script is necessary. See the example below for a PHP cURL example.

# Methods
 - `btsdk.sos.init(opts)` - Start the SOS module
 - `btsdk.sos.destroy()` - Destroy the SOS module.
 - `btsdk.sos.updateToken(token)` - Updates the single-use token.
 - `btsdk.sos.getCallID()` - Returns the current call ID.
 - `btsdk.sos.getCallAttachedData()` - Returns the call-attached-data for the current call.
 - `btsdk.sos.setCallAttachedData(key, value)` - Sets a call-attached-data key-value pair.

## btsdk.sos.init(opts)

Initializes the SIP engine and attaches the SOS widget to the page.

`opts` _object_ - An object containing the SDK initialization options. See below.

`opts.address` *string* [**required**] - The endpoint address.

Example: May use full address `c1e23a456@edge.humach.com` or just `c1e23a456`.

`opts.video` *boolean* [**optional**] - If set to `true`, will enable video calling.

Default: `false`

`opts.selectors` *object* [**optional**] - An object containing selector IDs for local and remote audio/video elements.

`opts.selectors.streamRemote` *string* [**optional**] - The remote `<audio>` or `<video>` element ID.

Default: `"btsdk-stream-remote"`

`opts.selectors.streamLocal`  *string* [**optional**] - The local `<audio>` or `<video>` element ID.

Default: `"btsdk-stream-local"`

`opts.listeners` *object* [**optional**] - An object containing event listeners.

`sos` *object* [**required**] - An object containing SOS-specific options.

`sos.selectorId` *string* [**required**] - The CSS selector ID to attach the widget to.

`sos.number` *string* [**required**] - The extension or PSTN number the SOS widget dials.

`sos.token` *string* [**required**] - The single-use token that authenticates the first outbound call. Subsequent outbound calls need a new token set via `btsdk.sos.updateToken(token)`.


## btsdk.sos.destroy()

Stops the SIP engine, disconnects from the WebSocket and removes the SOS widget from the page.

## btsdk.sos.updateToken(token)

`token` _string_ - A string containing the new token.

Updates the SOS password token.

## btsdk.sos.getCallID()

Proxy to `btsdk.sip.getCallID()`.

If a call is in progress, it will return a UUID identifying the call. Otherwise, it will return `null`.

## btsdk.sos.getCallAttachedData()

Proxy to `btsdk.api.getCallAttachedData()`.

If a call is in progress, it will fetch all call-attached-data that is attached to the current call. Returns a jQuery `jqXHR` object.

## btsdk.sos.setCallAttachedData(key, value)

`key` _string_ - A unique data key name.

`value` _mixed_ - The data content. Can be a string, number, or even JSON object.

If a call is in progress, it will set the given `key` to the `value`. Returns a jQuery `jqXHR` object.

**Note**: Both `btsdk.sos.getCallAttachedData()` and `btsdk.sos.setCallAttachedData(key, value)` are asynchronous and return a jQuery `jqXHR` object (http://api.jquery.com/Types/#jqXHR).

#### Example

```
// Setting call-attached-data
var xhr = btsdk.sos.setCallAttachedData("item_sku", "abc123");

xhr.done(function (data) {
  // call-attached-data successfully set
})
.fail(function ()  {
  console.error("Error setting call-attached-data");
});

.
.
.

// Fetching call-attached-data
var xhr = btsdk.sos.getCallAttachedData();

xhr.done(function (data) {
  var cad = data.result.data;

  for(i = 0; i < cad.length; i++) {
    console.log("key",cad[i].key,"value",cad[i].value);
  }
})
.fail(function ()  {
  console.error("Error fetching call-attached-data");
});
```

# Events
`btsdk.sos.initialized` - Fired once the SOS module is finished initializing.

`btsdk.sos.destroyed` - Fired once the SOS module is destroyed.

`btsdk.sos.error` - Fired when an error occurs int he SOS module.

# Examples

**Note**: Replace the `ENDPOINT_ADDRESS` and `ENDPOINT_ADDRESS_PASSWORD` placeholders with your values.

#### Generating a single-use token (server-side)

The endpoint address and password are used as Basic Auth credentials to generate the token.

```
<?php

// endpoint address credentials
$epa      = "ENDPOINT_ADDRESS@edge.humach.com";
$password = "ENDPOINT_ADDRESS_PASSWORD";

// request a single-use token
$url      = "https://api-edge.humach.com/edge/rest/endpointAddress/" . urlencode($epa);
$headers  = array("Content-Type: application/json");
$httpBody = json_encode(array(
              "method" => "generateToken",
              "ver"    => "latest",
              "format" => "json"
            ));

$ch = curl_init();

// endpoint address & password are used as basic-auth creds
curl_setopt($ch, CURLOPT_USERPWD, "{$epa}:{$password}");
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $httpBody);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$data = curl_exec($ch);
curl_close($ch);

$response = json_decode($data);

$token = isset($response->result->generateToken->token)
    ? $response->result->generateToken->token : null;
```

The value of `$token` needs to be passed to your JavaScript code. Something like this would work.

```
<script>
var btSosToken = <?= is_null($token) ? 'null' : "'{$token}'"; ?>;
</script>
```

#### Loading and initializing (client-side)

Once the SDK is loaded, `window.btAsyncInit()` is called. Place your code here to initialize the SDK with your options. The `$token` variable from the server needs to be available on the page.

```
<script>
window.btAsyncInit = function () {
  // ensure we have an acceptable browser and valid token
  if(btsdk.sos.acceptableBrowser() == false || !btSosToken) return;

  // init sdk with address, number-to-dial and single-use token
  btsdk.init({
    address: "ENDPOINT_ADDRESS",
    sos: {
      selectorId: "bt-sos",
      number: "NUMBER_TO_DIAL",
      token: btSosToken
    }
  });

  // start the sos module and show the widget
  btsdk.sos.init();
};

// async load the sdk
(function () {
  var script = document.createElement("script");
  script.src = "//code.humach.com/code/btsdk-2.18.0.min.js";
  script.async = true;
  var e = document.getElementsByTagName("script")[0];
  e.parentNode.insertBefore(script, e);
})();
</script>
```
